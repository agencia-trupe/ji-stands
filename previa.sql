-- MySQL dump 10.13  Distrib 5.7.10, for Linux (x86_64)
--
-- Host: localhost    Database: ji-stands
-- ------------------------------------------------------
-- Server version	5.7.10

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `frase_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `frase_en` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,1,'<strong>Stands</strong> para feiras e eventos de neg&oacute;cios','<strong>Stands</strong> para feiras e eventos de neg&oacute;cios','1_20160823183729.jpg','2016-08-23 18:37:30','2016-08-23 18:37:30'),(2,2,'<strong>Cenografia</strong> para o que a imagina&ccedil;&atilde;o mandar','<strong>Cenografia</strong> para o que a imagina&ccedil;&atilde;o mandar','2_20160823183805.jpg','2016-08-23 18:38:06','2016-08-23 18:38:06');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (3,0,'trupe_20160823185059.png','2016-08-23 18:50:59','2016-08-23 18:50:59');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `endereco_en` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `facebook_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `facebook_texto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `instagram_texto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `youtube_texto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (1,'contato@jistands.com.br','+55 11 2252 3166','Rua Pedra da Gal&eacute;, 33&nbsp;&middot; Vila Guilherme<br />\r\n02068-070&nbsp;&middot; S&atilde;o Paulo','Rua Pedra da Gal&eacute;, 33&nbsp;&middot; Vila Guilherme<br />\r\n02068-070&nbsp;&middot; S&atilde;o Paulo','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3658.6497123010176!2d-46.615124285023306!3d-23.50912328470887!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce58989ca7d627%3A0x3c7fc1e5c5172554!2sR.+Pedra+da+Gal%C3%A9%2C+33+-+Carandiru%2C+S%C3%A3o+Paulo+-+SP%2C+02068-070!5e0!3m2!1spt-BR!2sbr!4v1471978373689\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>','https://www.facebook.com/jistands/','fb.com/jistands','https://www.instagram.com/ji_stands/','@ji_stands','','',NULL,'2016-08-23 18:54:55');
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos_recebidos`
--

DROP TABLE IF EXISTS `contatos_recebidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `empresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos_recebidos`
--

LOCK TABLES `contatos_recebidos` WRITE;
/*!40000 ALTER TABLE `contatos_recebidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contatos_recebidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `diferenciais`
--

DROP TABLE IF EXISTS `diferenciais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `diferenciais` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `diferenciais`
--

LOCK TABLES `diferenciais` WRITE;
/*!40000 ALTER TABLE `diferenciais` DISABLE KEYS */;
INSERT INTO `diferenciais` VALUES (1,1,'diferenciais1_20160823184202.png','Lorem ipsum dolor sit','Lorem ipsum dolor sit','Praesent orci elit, pretium vel venenatis quis, gravida sit amet tortor. Vestibulum nisl ligula, scelerisque pretium purus vitae, lacinia semper mi.','Praesent orci elit, pretium vel venenatis quis, gravida sit amet tortor. Vestibulum nisl ligula, scelerisque pretium purus vitae, lacinia semper mi.','2016-08-23 18:42:02','2016-08-23 18:42:02'),(2,2,'diferenciais2_20160823184233.png','Lorem ipsum dolor sit','Lorem ipsum dolor sit','Praesent orci elit, pretium vel venenatis quis, gravida sit amet tortor. Vestibulum nisl ligula, scelerisque pretium purus vitae, lacinia semper mi.','Praesent orci elit, pretium vel venenatis quis, gravida sit amet tortor. Vestibulum nisl ligula, scelerisque pretium purus vitae, lacinia semper mi.','2016-08-23 18:42:33','2016-08-23 18:42:33'),(3,3,'diferenciais3_20160823184241.png','Lorem ipsum dolor sit','Lorem ipsum dolor sit','Praesent orci elit, pretium vel venenatis quis, gravida sit amet tortor. Vestibulum nisl ligula, scelerisque pretium purus vitae, lacinia semper mi.','Praesent orci elit, pretium vel venenatis quis, gravida sit amet tortor. Vestibulum nisl ligula, scelerisque pretium purus vitae, lacinia semper mi.','2016-08-23 18:42:41','2016-08-23 18:42:41'),(4,4,'diferenciais4_20160823184247.png','Lorem ipsum dolor sit','Lorem ipsum dolor sit','Praesent orci elit, pretium vel venenatis quis, gravida sit amet tortor. Vestibulum nisl ligula, scelerisque pretium purus vitae, lacinia semper mi.','Praesent orci elit, pretium vel venenatis quis, gravida sit amet tortor. Vestibulum nisl ligula, scelerisque pretium purus vitae, lacinia semper mi.','2016-08-23 18:42:47','2016-08-23 18:42:47'),(5,5,'diferenciais5_20160823184254.png','Lorem ipsum dolor sit','Lorem ipsum dolor sit','Praesent orci elit, pretium vel venenatis quis, gravida sit amet tortor. Vestibulum nisl ligula, scelerisque pretium purus vitae, lacinia semper mi.','Praesent orci elit, pretium vel venenatis quis, gravida sit amet tortor. Vestibulum nisl ligula, scelerisque pretium purus vitae, lacinia semper mi.','2016-08-23 18:42:54','2016-08-23 18:42:54');
/*!40000 ALTER TABLE `diferenciais` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historia`
--

DROP TABLE IF EXISTS `historia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historia`
--

LOCK TABLES `historia` WRITE;
/*!40000 ALTER TABLE `historia` DISABLE KEYS */;
INSERT INTO `historia` VALUES (1,'<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tincidunt diam ipsum, at fringilla turpis luctus in. In a ipsum ligula. Nullam imperdiet urna sagittis sapien facilisis porta. Ut quis ornare diam. Integer mattis nunc tellus, eget condimentum neque congue rutrum. Curabitur consectetur, urna eu sagittis consequat, neque lorem pellentesque dui, in vulputate dui nisl non tellus.</p>\r\n\r\n<p><img src=\"http://ji-stands.dev/assets/img/geral/img-precisao-criatividade_20160823183858.png\" /></p>\r\n\r\n<p>Nulla laoreet quam suscipit placerat feugiat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum dignissim dignissim felis et tincidunt. Pellentesque eu eros justo. Morbi non scelerisque arcu, et lobortis arcu.</p>\r\n','<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis tincidunt diam ipsum, at fringilla turpis luctus in. In a ipsum ligula. Nullam imperdiet urna sagittis sapien facilisis porta. Ut quis ornare diam. Integer mattis nunc tellus, eget condimentum neque congue rutrum. Curabitur consectetur, urna eu sagittis consequat, neque lorem pellentesque dui, in vulputate dui nisl non tellus.</p>\r\n\r\n<p><img src=\"http://ji-stands.dev/assets/img/geral/img-precisao-criatividade_20160823183858.png\" /></p>\r\n\r\n<p>Nulla laoreet quam suscipit placerat feugiat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Vestibulum dignissim dignissim felis et tincidunt. Pellentesque eu eros justo. Morbi non scelerisque arcu, et lobortis arcu.</p>\r\n',NULL,'2016-08-23 18:39:04');
/*!40000 ALTER TABLE `historia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2016_02_01_000000_create_users_table',1),('2016_03_01_000000_create_contato_table',1),('2016_03_01_000000_create_contatos_recebidos_table',1),('2016_08_16_174221_create_banners_table',1),('2016_08_16_175629_create_servicos_table',1),('2016_08_16_180038_create_segmentos_table',1),('2016_08_16_181025_create_historia_table',1),('2016_08_16_181917_create_diferenciais_table',1),('2016_08_17_140147_create_projetos_table',1),('2016_08_17_144215_create_clientes_table',1),('2016_08_17_145546_create_newsletter_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletter`
--

DROP TABLE IF EXISTS `newsletter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsletter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `newsletter_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletter`
--

LOCK TABLES `newsletter` WRITE;
/*!40000 ALTER TABLE `newsletter` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projetos`
--

DROP TABLE IF EXISTS `projetos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projetos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `categoria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projetos`
--

LOCK TABLES `projetos` WRITE;
/*!40000 ALTER TABLE `projetos` DISABLE KEYS */;
INSERT INTO `projetos` VALUES (1,0,'stands','Exemplo','Exemplo','1_20160823184540.jpg','2016-08-23 18:45:40','2016-08-23 18:45:40');
/*!40000 ALTER TABLE `projetos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `projetos_imagens`
--

DROP TABLE IF EXISTS `projetos_imagens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projetos_imagens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projeto_id` int(10) unsigned NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `projetos_imagens_projeto_id_foreign` (`projeto_id`),
  CONSTRAINT `projetos_imagens_projeto_id_foreign` FOREIGN KEY (`projeto_id`) REFERENCES `projetos` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `projetos_imagens`
--

LOCK TABLES `projetos_imagens` WRITE;
/*!40000 ALTER TABLE `projetos_imagens` DISABLE KEYS */;
INSERT INTO `projetos_imagens` VALUES (1,1,0,'2_20160823184550.jpg','2016-08-23 18:45:51','2016-08-23 18:45:51'),(2,1,0,'1_20160823184550.jpg','2016-08-23 18:45:51','2016-08-23 18:45:51');
/*!40000 ALTER TABLE `projetos_imagens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `segmentos`
--

DROP TABLE IF EXISTS `segmentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `segmentos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo_pt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `segmentos`
--

LOCK TABLES `segmentos` WRITE;
/*!40000 ALTER TABLE `segmentos` DISABLE KEYS */;
INSERT INTO `segmentos` VALUES (1,1,'Arquitetura Promocional','Arquitetura Promocional','2016-08-23 18:44:35','2016-08-23 18:44:35'),(2,2,'PDV','PDV','2016-08-23 18:44:42','2016-08-23 18:44:42'),(3,3,'Quiosques de Shopping','Quiosques de Shopping','2016-08-23 18:44:55','2016-08-23 18:44:55'),(4,4,'Lançamento de Produto','Lançamento de Produto','2016-08-23 18:45:05','2016-08-23 18:45:05');
/*!40000 ALTER TABLE `segmentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `servicos`
--

DROP TABLE IF EXISTS `servicos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `servicos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `stands_imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `stands_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `stands_en` text COLLATE utf8_unicode_ci NOT NULL,
  `cenografia_imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cenografia_pt` text COLLATE utf8_unicode_ci NOT NULL,
  `cenografia_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `servicos`
--

LOCK TABLES `servicos` WRITE;
/*!40000 ALTER TABLE `servicos` DISABLE KEYS */;
INSERT INTO `servicos` VALUES (1,'1_20160823184405.jpg','<ul>\r\n	<li>Praesent orci elit, pretium vel venenatis quis.</li>\r\n	<li>Gravida sit amet tortor.</li>\r\n	<li>Vestibulum nisl ligula, scelerisque pretium.</li>\r\n	<li>Purus vitae, lacinia semper mi.</li>\r\n</ul>\r\n','<ul>\r\n	<li>Praesent orci elit, pretium vel venenatis quis.</li>\r\n	<li>Gravida sit amet tortor.</li>\r\n	<li>Vestibulum nisl ligula, scelerisque pretium.</li>\r\n	<li>Purus vitae, lacinia semper mi.</li>\r\n</ul>\r\n','2_20160823184405.jpg','<ul>\r\n	<li>Praesent orci elit, pretium vel venenatis quis.</li>\r\n	<li>Gravida sit amet tortor.</li>\r\n	<li>Vestibulum nisl ligula, scelerisque pretium.</li>\r\n	<li>Purus vitae, lacinia semper mi.</li>\r\n</ul>\r\n','<ul>\r\n	<li>Praesent orci elit, pretium vel venenatis quis.</li>\r\n	<li>Gravida sit amet tortor.</li>\r\n	<li>Vestibulum nisl ligula, scelerisque pretium.</li>\r\n	<li>Purus vitae, lacinia semper mi.</li>\r\n</ul>\r\n',NULL,'2016-08-23 18:44:05');
/*!40000 ALTER TABLE `servicos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'trupe','contato@trupe.net','$2y$10$kpYKsMrvzfkfnJ/kbwt4v.GSYxhBCmbH.UIAAWRV0kM.Lxm0P.g/O','GUuLI8AtpgH2I6ILqcAYjEUjoPDOXBMzjn7G4dRJQ62A60jEoPvbDTzTeNEV',NULL,'2016-08-23 18:36:58');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-23 18:55:57
