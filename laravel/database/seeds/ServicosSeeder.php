<?php

use Illuminate\Database\Seeder;

class ServicosSeeder extends Seeder
{
    public function run()
    {
        DB::table('servicos')->insert([
            'stands_imagem' => '',
            'stands_pt' => '',
            'stands_en' => '',
            'cenografia_imagem' => '',
            'cenografia_pt' => '',
            'cenografia_en' => '',
        ]);
    }
}
