<?php

use Illuminate\Database\Seeder;

class ValoresSeeder extends Seeder
{
    public function run()
    {
        DB::table('valores')->insert([
            'missao_pt' => '',
            'missao_en' => '',
            'visao_pt' => '',
            'visao_en' => '',
            'valores_pt' => '',
            'valores_en' => '',
        ]);
    }
}
