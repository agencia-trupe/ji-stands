<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiferenciaisTable extends Migration
{
    public function up()
    {
        Schema::create('diferenciais', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('imagem');
            $table->string('titulo_pt');
            $table->string('titulo_en');
            $table->text('texto_pt');
            $table->text('texto_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('diferenciais');
    }
}
