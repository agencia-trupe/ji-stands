<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePopupTable extends Migration
{
    public function up()
    {
        Schema::create('popup', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('ativo');
            $table->string('imagem');
            $table->string('link');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('popup');
    }
}
