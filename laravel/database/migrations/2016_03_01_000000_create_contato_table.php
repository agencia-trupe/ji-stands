<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContatoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contato', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('telefone');
            $table->text('endereco_pt');
            $table->text('endereco_en');
            $table->text('google_maps');
            $table->string('facebook_link');
            $table->string('facebook_texto');
            $table->string('instagram_link');
            $table->string('instagram_texto');
            $table->string('youtube_link');
            $table->string('youtube_texto');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('contato');
    }
}
