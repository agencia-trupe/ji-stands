<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicosTable extends Migration
{
    public function up()
    {
        Schema::create('servicos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('stands_imagem');
            $table->text('stands_pt');
            $table->text('stands_en');
            $table->string('cenografia_imagem');
            $table->text('cenografia_pt');
            $table->text('cenografia_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('servicos');
    }
}
