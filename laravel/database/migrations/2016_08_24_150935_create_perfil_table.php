<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerfilTable extends Migration
{
    public function up()
    {
        Schema::create('perfil', function (Blueprint $table) {
            $table->increments('id');
            $table->text('jens_pt');
            $table->text('jens_en');
            $table->text('ivair_pt');
            $table->text('ivair_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('perfil');
    }
}
