<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValoresTable extends Migration
{
    public function up()
    {
        Schema::create('valores', function (Blueprint $table) {
            $table->increments('id');
            $table->text('missao_pt');
            $table->text('missao_en');
            $table->text('visao_pt');
            $table->text('visao_en');
            $table->text('valores_pt');
            $table->text('valores_en');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('valores');
    }
}
