<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('frontend.common.*', function($view) {
            $view->with('contato', \App\Models\Contato::first());
        });

        view()->composer('painel.common.nav', function($view) {
            $view->with('contatosNaoLidos', \App\Models\ContatoRecebido::naoLidos()->count());
            $view->with('fornecedoresNaoLidos', \App\Models\FornecedorRecebido::naoLidos()->count());
            $view->with('curriculosNaoLidos', \App\Models\CurriculoRecebido::naoLidos()->count());
            $view->with('briefingsNaoLidos', \App\Models\BriefingRecebido::naoLidos()->count());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
