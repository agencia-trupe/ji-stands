<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
		$router->model('popup', 'App\Models\Popup');
		$router->model('depoimentos', 'App\Models\Depoimento');
		$router->model('perfil', 'App\Models\Perfil');
		$router->model('estrutura', 'App\Models\Estrutura');
		$router->model('valores', 'App\Models\Valores');
		$router->model('clientes', 'App\Models\Cliente');
        $router->model('projetos', 'App\Models\Projeto');
		$router->model('imagens', 'App\Models\ProjetoImagem');
		$router->model('diferenciais', 'App\Models\Diferencial');
		$router->model('historia', 'App\Models\Historia');
		$router->model('segmentos', 'App\Models\Segmento');
		$router->model('servicos', 'App\Models\Servicos');
		$router->model('banners', 'App\Models\Banner');
        $router->model('recebidos', 'App\Models\ContatoRecebido');
        $router->model('fornecedores', 'App\Models\FornecedorRecebido');
        $router->model('curriculos', 'App\Models\CurriculoRecebido');
        $router->model('briefings', 'App\Models\BriefingRecebido');
        $router->model('contato', 'App\Models\Contato');
        $router->model('newsletter', 'App\Models\Newsletter');
        $router->model('usuarios', 'App\Models\User');

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
