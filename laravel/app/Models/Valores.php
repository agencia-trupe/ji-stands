<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Valores extends Model
{
    protected $table = 'valores';

    protected $guarded = ['id'];

}
