<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Historia extends Model
{
    protected $table = 'historia';

    protected $guarded = ['id'];

}
