<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Servicos extends Model
{
    protected $table = 'servicos';

    protected $guarded = ['id'];

    public static function upload_stands_imagem()
    {
        return CropImage::make('stands_imagem', [
            'width'  => 550,
            'height' => 250,
            'path'   => 'assets/img/servicos/'
        ]);
    }

    public static function upload_cenografia_imagem()
    {
        return CropImage::make('cenografia_imagem', [
            'width'  => 550,
            'height' => 250,
            'path'   => 'assets/img/servicos/'
        ]);
    }

}
