<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Cliente extends Model
{
    protected $table = 'clientes';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('imagem', 'ASC');
    }

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => 172,
            'height' => 114,
            'bg'     => '#fff',
            'path'   => 'assets/img/clientes/'
        ]);
    }

}
