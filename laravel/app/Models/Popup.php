<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Helpers\CropImage;

class Popup extends Model
{
    protected $table = 'popup';

    protected $guarded = ['id'];

    public static function upload_imagem()
    {
        return CropImage::make('imagem', [
            'width'  => null,
            'height' => null,
            'path'   => 'assets/img/popup/'
        ]);
    }

}
