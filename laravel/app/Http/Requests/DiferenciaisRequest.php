<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DiferenciaisRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'imagem' => 'required|image',
            'titulo_pt' => 'required',
            'texto_pt' => 'required',
            'titulo_en' => 'required',
            'texto_en' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
