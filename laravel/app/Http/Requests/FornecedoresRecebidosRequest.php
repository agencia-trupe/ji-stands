<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FornecedoresRecebidosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'     => 'required',
            'email'    => 'required|email',
            'mensagem' => 'required',
        ];
    }

    public function messages() {
        return [
            'required' => trans('frontend.contato.erro'),
            'email'    => trans('frontend.contato.erro'),
        ];
    }
}
