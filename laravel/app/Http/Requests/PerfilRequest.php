<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PerfilRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'jens_pt' => 'required',
            'jens_en' => 'required',
            'ivair_pt' => 'required',
            'ivair_en' => 'required',
        ];
    }
}
