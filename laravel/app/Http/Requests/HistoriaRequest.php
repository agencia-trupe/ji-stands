<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HistoriaRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'texto_pt' => 'required',
            'texto_en' => 'required',
        ];
    }
}
