<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BriefingsRecebidosRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'          => 'required',
            'email'         => 'required|email',
            'como_conheceu' => 'required',
            'briefing'      => 'mimes:doc,docx,pdf,txt,rtf,zip,xls,xlsx,application/csv,application/excel,application/vnd.ms-excel,application/vnd.msexcel,text/csv|max:5000'
        ];
    }

    public function messages() {
        return [
            'required' => trans('frontend.contato.erro'),
            'email'    => trans('frontend.contato.erro'),
        ];
    }
}
