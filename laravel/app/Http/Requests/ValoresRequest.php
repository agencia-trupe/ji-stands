<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ValoresRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'missao_pt' => 'required',
            'missao_en' => 'required',
            'visao_pt' => 'required',
            'visao_en' => 'required',
            'valores_pt' => 'required',
            'valores_en' => 'required',
        ];
    }
}
