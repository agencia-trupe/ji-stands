<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ServicosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'stands_imagem' => 'image',
            'stands_pt' => 'required',
            'stands_en' => 'required',
            'cenografia_imagem' => 'image',
            'cenografia_pt' => 'required',
            'cenografia_en' => 'required',
        ];
    }
}
