<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DepoimentosRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo_pt' => 'required',
            'titulo_en' => 'required',
            'descricao_pt' => 'required',
            'descricao_en' => 'required',
            'video_tipo' => 'required',
            'video_codigo' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
