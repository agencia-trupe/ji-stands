<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('empresa/{secao?}', 'EmpresaController@index')->name('empresa');
    Route::get('servicos', 'ServicosController@index')->name('servicos');
    Route::get('projetos/{categoria?}', 'ProjetosController@index')->name('projetos');
    Route::get('clientes', 'ClientesController@index')->name('clientes');
    Route::get('contato', 'ContatoController@index')->name('contato');
    Route::post('contato/contato', 'ContatoController@postContato')->name('contato.post-contato');
    Route::post('contato/fornecedor', 'ContatoController@postFornecedor')->name('contato.post-fornecedor');
    Route::post('contato/briefing', 'ContatoController@postBriefing')->name('contato.post-briefing');
    Route::post('contato/curriculo', 'ContatoController@postCurriculo')->name('contato.post-curriculo');
    Route::post('newsletter', 'NewsletterController@post')->name('newsletter.post');


    // Localização
    Route::get('lang/{idioma?}', function ($idioma = 'pt') {
        if ($idioma == 'pt' || $idioma == 'en') Session::put('locale', $idioma);
        return redirect()->back();
    })->name('lang');


    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('popup', 'PopupController', ['only' => ['index', 'update']]);
		Route::resource('depoimentos', 'DepoimentosController');
		Route::resource('perfil', 'PerfilController', ['only' => ['index', 'update']]);
		Route::resource('estrutura', 'EstruturaController');
		Route::resource('valores', 'ValoresController', ['only' => ['index', 'update']]);
		Route::resource('clientes', 'ClientesController');
		Route::resource('projetos', 'ProjetosController');
        Route::get('projetos/{projetos}/imagens/clear', [
            'as'   => 'painel.projetos.imagens.clear',
            'uses' => 'ProjetosImagensController@clear'
        ]);
        Route::resource('projetos.imagens', 'ProjetosImagensController');
		Route::resource('diferenciais', 'DiferenciaisController');
		Route::resource('historia', 'HistoriaController', ['only' => ['index', 'update']]);
		Route::resource('segmentos', 'SegmentosController');
		Route::resource('servicos', 'ServicosController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato/fornecedores', 'FornecedoresRecebidosController');
        Route::resource('contato/curriculos', 'CurriculosRecebidosController');
        Route::resource('contato/briefings', 'BriefingsRecebidosController');
        Route::resource('contato', 'ContatoController');
        Route::get('newsletter/exportar', ['as' => 'painel.newsletter.exportar', 'uses' => 'NewsletterController@exportar']);
        Route::resource('newsletter', 'NewsletterController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('order', 'PainelController@order');
        Route::post('ckeditor-upload', 'PainelController@imageUpload');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
