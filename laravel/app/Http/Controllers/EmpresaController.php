<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Historia;
use App\Models\Diferencial;
use App\Models\Valores;
use App\Models\Estrutura;
use App\Models\Perfil;
use App\Models\Depoimento;

class EmpresaController extends Controller
{
    public function index($secao = null)
    {
        $historia     = Historia::first();
        $diferenciais = Diferencial::ordenados()->get();
        $valores      = Valores::first();
        $estruturas   = Estrutura::ordenados()->get();
        $perfil       = Perfil::first();
        $depoimentos  = Depoimento::ordenados()->get();

        return view('frontend.empresa', compact('historia', 'diferenciais', 'valores', 'estruturas', 'perfil', 'depoimentos', 'secao'));
    }
}
