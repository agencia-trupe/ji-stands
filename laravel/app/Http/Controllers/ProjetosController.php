<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Projeto;

class ProjetosController extends Controller
{
    public function index($categoria = 'stands')
    {
        if ($categoria != 'stands' && $categoria != 'cenografia') abort('404');

        $projetos = Projeto::whereCategoria($categoria)->ordenados()->get();

        view()->share(compact('categoria'));
        return view('frontend.projetos', compact('projetos'));
    }
}
