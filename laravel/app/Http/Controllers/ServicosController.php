<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Models\Servicos;
use App\Models\Segmento;

class ServicosController extends Controller
{
    public function index()
    {
        $servicos = Servicos::first();
        $segmentos = Segmento::ordenados()->get();

        return view('frontend.servicos', compact('servicos', 'segmentos'));
    }
}
