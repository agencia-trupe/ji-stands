<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\HistoriaRequest;
use App\Http\Controllers\Controller;

use App\Models\Historia;

class HistoriaController extends Controller
{
    public function index()
    {
        $registro = Historia::first();

        return view('painel.historia.edit', compact('registro'));
    }

    public function update(HistoriaRequest $request, Historia $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.historia.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
