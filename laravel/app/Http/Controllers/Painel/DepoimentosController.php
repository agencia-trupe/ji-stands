<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DepoimentosRequest;
use App\Http\Controllers\Controller;

use App\Models\Depoimento;
use App\Helpers\Tools;

class DepoimentosController extends Controller
{
    public function index()
    {
        $registros = Depoimento::ordenados()->get();

        return view('painel.depoimentos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.depoimentos.create');
    }

    public function store(DepoimentosRequest $request)
    {
        try {

            $input = $request->all();
            $input['capa'] = Tools::videoThumb($request->get('video_tipo'), $request->get('video_codigo'));

            Depoimento::create($input);

            return redirect()->route('painel.depoimentos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Depoimento $registro)
    {
        return view('painel.depoimentos.edit', compact('registro'));
    }

    public function update(DepoimentosRequest $request, Depoimento $registro)
    {
        try {

            $input = $request->all();

            if ($input['video_codigo'] !== $registro->video_codigo || $input['video_tipo'] !== $registro->video_tipo) {
                $input['capa'] = Tools::videoThumb($input['video_tipo'], $input['video_codigo']);
            }

            $registro->update($input);

            return redirect()->route('painel.depoimentos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Depoimento $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.depoimentos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
