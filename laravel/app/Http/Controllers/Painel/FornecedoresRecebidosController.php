<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\FornecedorRecebido;

class FornecedoresRecebidosController extends Controller
{
    public function index()
    {
        $contatosrecebidos = FornecedorRecebido::orderBy('id', 'DESC')->paginate(15);

        return view('painel.contato.fornecedores.index', compact('contatosrecebidos'));
    }

    public function show(FornecedorRecebido $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.contato.fornecedores.show', compact('contato'));
    }

    public function destroy(FornecedorRecebido $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.contato.fornecedores.index')->with('success', 'Mensagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }
}
