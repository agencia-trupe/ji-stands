<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\CurriculoRecebido;

class CurriculosRecebidosController extends Controller
{
    public function index()
    {
        $contatosrecebidos = CurriculoRecebido::orderBy('id', 'DESC')->paginate(15);

        return view('painel.contato.curriculos.index', compact('contatosrecebidos'));
    }

    public function show(CurriculoRecebido $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.contato.curriculos.show', compact('contato'));
    }

    public function destroy(CurriculoRecebido $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.contato.curriculos.index')->with('success', 'Mensagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }
}
