<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ServicosRequest;
use App\Http\Controllers\Controller;

use App\Models\Servicos;

class ServicosController extends Controller
{
    public function index()
    {
        $registro = Servicos::first();

        return view('painel.servicos.edit', compact('registro'));
    }

    public function update(ServicosRequest $request, Servicos $registro)
    {
        try {
            $input = $request->all();

            if (isset($input['stands_imagem'])) $input['stands_imagem'] = Servicos::upload_stands_imagem();
            if (isset($input['cenografia_imagem'])) $input['cenografia_imagem'] = Servicos::upload_cenografia_imagem();

            $registro->update($input);

            return redirect()->route('painel.servicos.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
