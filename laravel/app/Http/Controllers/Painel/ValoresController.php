<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ValoresRequest;
use App\Http\Controllers\Controller;

use App\Models\Valores;

class ValoresController extends Controller
{
    public function index()
    {
        $registro = Valores::first();

        return view('painel.valores.edit', compact('registro'));
    }

    public function update(ValoresRequest $request, Valores $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.valores.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
