<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\SegmentosRequest;
use App\Http\Controllers\Controller;

use App\Models\Segmento;

class SegmentosController extends Controller
{
    public function index()
    {
        $registros = Segmento::ordenados()->get();

        return view('painel.segmentos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.segmentos.create');
    }

    public function store(SegmentosRequest $request)
    {
        try {

            $input = $request->all();


            Segmento::create($input);
            return redirect()->route('painel.segmentos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Segmento $registro)
    {
        return view('painel.segmentos.edit', compact('registro'));
    }

    public function update(SegmentosRequest $request, Segmento $registro)
    {
        try {

            $input = $request->all();


            $registro->update($input);
            return redirect()->route('painel.segmentos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Segmento $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.segmentos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
