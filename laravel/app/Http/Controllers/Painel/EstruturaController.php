<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\EstruturaRequest;
use App\Http\Controllers\Controller;

use App\Models\Estrutura;

class EstruturaController extends Controller
{
    public function index()
    {
        $registros = Estrutura::ordenados()->get();

        return view('painel.estrutura.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.estrutura.create');
    }

    public function store(EstruturaRequest $request)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Estrutura::upload_imagem();

            Estrutura::create($input);
            return redirect()->route('painel.estrutura.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Estrutura $registro)
    {
        return view('painel.estrutura.edit', compact('registro'));
    }

    public function update(EstruturaRequest $request, Estrutura $registro)
    {
        try {

            $input = $request->all();

            if (isset($input['imagem'])) $input['imagem'] = Estrutura::upload_imagem();

            $registro->update($input);
            return redirect()->route('painel.estrutura.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Estrutura $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.estrutura.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
