<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ClientesRequest;
use App\Http\Controllers\Controller;

use App\Models\Cliente;

class ClientesController extends Controller
{
    public function index()
    {
        $clientes = Cliente::ordenados()->get();

        return view('painel.clientes.index', compact('clientes'));
    }

    public function show(Cliente $cliente)
    {
        return $cliente;
    }

    public function store(ClientesRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = Cliente::upload_imagem();

            $cliente = Cliente::create($input);

            $view = view('painel.clientes.imagem', compact('cliente'))->render();

            return response()->json(['body' => $view]);

        } catch (\Exception $e) {

            return 'Erro ao adicionar imagem: '.$e->getMessage();

        }
    }

    public function destroy(Cliente $cliente)
    {
        try {

            $cliente->delete();
            return redirect()->route('painel.clientes.index')
                             ->with('success', 'Imagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir imagem: '.$e->getMessage()]);

        }
    }
}
