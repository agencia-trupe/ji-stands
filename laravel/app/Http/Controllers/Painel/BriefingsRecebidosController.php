<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\BriefingRecebido;

class BriefingsRecebidosController extends Controller
{
    public function index()
    {
        $contatosrecebidos = BriefingRecebido::orderBy('id', 'DESC')->paginate(15);

        return view('painel.contato.briefings.index', compact('contatosrecebidos'));
    }

    public function show(BriefingRecebido $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.contato.briefings.show', compact('contato'));
    }

    public function destroy(BriefingRecebido $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.contato.briefings.index')->with('success', 'Mensagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }
}
