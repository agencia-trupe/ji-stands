<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Models\Popup;

class HomeController extends Controller
{
    public function index()
    {
        $banners = Banner::ordenados()->get();
        $popup   = Popup::first();

        return view('frontend.home', compact('banners', 'popup'));
    }
}
