<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ContatosRecebidosRequest;
use App\Http\Requests\FornecedoresRecebidosRequest;
use App\Http\Requests\BriefingsRecebidosRequest;
use App\Http\Requests\CurriculosRecebidosRequest;

use App\Models\Contato;
use App\Models\ContatoRecebido;
use App\Models\Fornecedor;
use App\Models\FornecedorRecebido;
use App\Models\Briefing;
use App\Models\BriefingRecebido;
use App\Models\Curriculo;
use App\Models\CurriculoRecebido;

class ContatoController extends Controller
{
    public function index()
    {
        $contato = Contato::first();

        return view('frontend.contato', compact('contato'));
    }

    public function postContato(ContatosRecebidosRequest $request, ContatoRecebido $contatoRecebido)
    {
        $contatoRecebido->create($request->all());

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.contato', $request->all(), function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[CONTATO] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => trans('frontend.contato.sucesso')
        ];

        return response()->json($response);
    }

    public function postFornecedor(FornecedoresRecebidosRequest $request, FornecedorRecebido $contatoRecebido)
    {
        $contatoRecebido->create($request->all());

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.fornecedor', $request->all(), function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[FORNECEDOR] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => trans('frontend.contato.sucesso')
        ];

        return response()->json($response);
    }

    public function postBriefing(BriefingsRecebidosRequest $request, BriefingRecebido $contatoRecebido)
    {
        $input = $request->all();

        if ($request->hasFile('briefing')) {
            $input['briefing'] = BriefingRecebido::upload_briefing();
        } else {
            $input['briefing'] = '';
        }

        $contatoRecebido->create($input);

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.briefing', $input, function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[BRIEFING] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => trans('frontend.contato.sucesso')
        ];

        return response()->json($response);
    }

    public function postCurriculo(CurriculosRecebidosRequest $request, CurriculoRecebido $contatoRecebido)
    {
        $input = $request->all();

        if ($request->hasFile('curriculo')) {
            $input['curriculo'] = CurriculoRecebido::upload_curriculo();
        } else {
            $input['curriculo'] = '';
        }

        $contatoRecebido->create($input);

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.curriculo', $input, function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[CURRÍCULO] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => trans('frontend.contato.sucesso')
        ];

        return response()->json($response);
    }
}
