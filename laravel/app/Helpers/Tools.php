<?php

namespace App\Helpers;

class Tools
{
    public static function videoThumb($tipo, $id)
    {
        if (!file_exists(public_path('assets/img/depoimentos/'))) {
            mkdir(public_path('assets/img/depoimentos/'), 0777, true);
        }

        try {
            if ($tipo == 'vimeo') {

                $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$id.php"));
                $thumbURL = isset($hash[0]['thumbnail_large']) ? $hash[0]['thumbnail_large'] : false;

                $filename = $id.'_'.date('YmdHis').'.jpg';
                copy($thumbURL, 'assets/img/depoimentos/'.$filename);

                return $filename;

            } elseif ($tipo == 'youtube') {

                $thumbURL = "http://img.youtube.com/vi/$id/mqdefault.jpg";

                $filename = $id.'_'.date('YmdHis').'.jpg';
                copy($thumbURL, 'assets/img/depoimentos/'.$filename);

                return $filename;

            }
        } catch (\Exception $e) {
            throw new \Exception('Erro ao obter imagem de capa, verifique o código do vídeo.', 1);
        }

        return '';
    }
}
