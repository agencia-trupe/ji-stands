(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.mobileToggle = function() {
        var $handle = $('#mobile-toggle'),
            $nav    = $('#nav-mobile');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });

        $('#nav-mobile .menu-projetos > a').on('click touchstart', function(event) {
            event.preventDefault();
            $(this).next().slideToggle();
        });
    };

    App.bannersHome = function() {
        var $handle = $('.banners');

        $handle.on('cycle-initialized', function(e, opt) {
            $('.cycle-slide-active').find('.imagem').addClass('animado');
        });

        $handle.on('cycle-before', function(e, opt, outSlide, inSlide) {
            $(inSlide).find('.imagem').addClass('animado');
            setTimeout(function() {
                $(outSlide).find('.imagem').removeClass('animado');
            }, 1300);
        });

        $handle.cycle({
            slides: '>.banner',
            speed: 1300,
            timeout: 5500
        });
    };

    App.popupHome = function() {
        $('#popup').fancybox({
            padding: 0,
            afterLoad: function(current) {
                if(!$('#popup').data('link')) return;
                $(current.inner).wrap($("<a />", {
                    href: $('#popup').data('link'),
                }));
            }
        }).trigger('click');
    };

    App.empresaSecoes = function() {
        var $handle = $('.secao .handle');

        $handle.on('click', function(event) {
            event.preventDefault();

            $(this).next().slideToggle();
            $(this).parent().toggleClass('aberto');

            if ($(this).parent().hasClass('aberto')) {
                $('html, body').animate({
                    scrollTop: $(this).offset().top
                }, 500);
            }
        });

        var aberto = $('.secao.aberto');
        if (aberto.length == 1) {
            $('html, body').animate({
                scrollTop: aberto.offset().top
            }, 500);
        }
    };

    App.empresaVideo = function() {
        $('.video').fancybox({
            padding: 0,
            type: 'iframe',
            width: 800,
            height: 450,
            aspectRatio: true
        });
    };

    App.projetosMasonry = function() {
        var $grid = $('.projetos-masonry');
        if (!$grid.length) return;

        $grid.waitForImages(function() {
            $grid.masonry({
                itemSelector: '.thumb-masonry',
                columnWidth: '.grid-sizer',
                gutter: '.gutter-sizer'
            });
        });
    };

    App.projetoLightbox = function() {
        $('.fancybox').fancybox({
            padding: 10,
            helpers: {
                title: null
            }
        });

        $('.thumb').click(function(e) {
            var el, id = $(this).data('galeria');
            if(id){
                el = $('.fancybox[rel=galeria' + id + ']:eq(0)');
                e.preventDefault();
                el.click();
            }
        });
    };

    App.envioNewsletter = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-newsletter-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/newsletter',
            data: {
                nome: $('#newsletter_nome').val(),
                email: $('#newsletter_email').val()
            },
            success: function(data) {
                $form[0].reset();
                $response.hide().text(data.message).fadeIn('slow');
            },
            error: function(data) {
                var errorMsg = data.responseJSON.nome ? data.responseJSON.nome : data.responseJSON.email;
                $response.hide().text(errorMsg).fadeIn('slow');
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };

    App.envioContato = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-contato-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato/contato',
            data: {
                nome: $('#contato_nome').val(),
                email: $('#contato_email').val(),
                telefone: $('#contato_telefone').val(),
                mensagem: $('#contato_mensagem').val(),
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
            },
            error: function(data) {
                if (data.responseJSON) {
                    var error = data.responseJSON[Object.keys(data.responseJSON)[0]];
                    $response.fadeOut().text(error).fadeIn('slow');
                }
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };

    App.envioFornecedor = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-fornecedor-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato/fornecedor',
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
                $form.find('.file-input input').trigger('change');
            },
            error: function(data) {
                if (data.responseJSON) {
                    var error = data.responseJSON[Object.keys(data.responseJSON)[0]];
                    $response.fadeOut().text(error).fadeIn('slow');
                }
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };

    App.envioBriefing = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-briefing-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato/briefing',
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
                $form.find('.file-input input').trigger('change');
            },
            error: function(data) {
                if (data.responseJSON) {
                    var error = data.responseJSON[Object.keys(data.responseJSON)[0]];
                    $response.fadeOut().text(error).fadeIn('slow');
                }
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };

    App.envioCurriculo = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-curriculo-response');

        if ($form.hasClass('sending')) return false;

        $response.fadeOut('fast');
        $form.addClass('sending');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato/curriculo',
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
                $form.find('.file-input input').trigger('change');
            },
            error: function(data) {
                if (data.responseJSON) {
                    var error = data.responseJSON[Object.keys(data.responseJSON)[0]];
                    $response.fadeOut().text(error).fadeIn('slow');
                }
            },
            dataType: 'json'
        })
        .always(function() {
            $form.removeClass('sending');
        });
    };

    App.fileInput = function() {
        $('.file-input').each(function(index, el) {
            $(el).data('title', $(el).find('span').html());
        });
        $('.file-input input').on('change', function(e) {
            if (e.target.files.length) {
                var fileName = e.target.files[0].name;
                $(this).parent().addClass('active');
                $(this).next().html(fileName);
            } else {
                $(this).parent().removeClass('active');
                $(this).next().html($(this).parent().data('title'));
            }
        });
    };

    App.init = function() {
        this.mobileToggle();
        this.bannersHome();
        this.popupHome();
        this.empresaSecoes();
        this.empresaVideo();
        this.projetosMasonry();
        this.projetoLightbox();
        $('#form-newsletter').on('submit', this.envioNewsletter);
        $('#form-contato').on('submit', this.envioContato);
        $('#form-fornecedor').on('submit', this.envioFornecedor);
        $('#form-briefing').on('submit', this.envioBriefing);
        $('#form-curriculo').on('submit', this.envioCurriculo);
        this.fileInput();
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
