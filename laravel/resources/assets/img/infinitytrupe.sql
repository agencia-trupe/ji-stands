-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: infinitytrupe.mysql.dbaas.com.br
-- Generation Time: 18-Ago-2016 às 19:47
-- Versão do servidor: 5.6.30-76.3-log
-- PHP Version: 5.6.24-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `infinitytrupe`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `comparativo`
--

CREATE TABLE `comparativo` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `comparativo` text COLLATE utf8_unicode_ci NOT NULL,
  `comparativo_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `comparativo`
--

INSERT INTO `comparativo` (`id`, `ordem`, `comparativo`, `comparativo_en`, `created_at`, `updated_at`) VALUES
(1, 0, 'Endereço corporativo privilegiado', 'Privileged corporative address ', '2016-03-16 15:15:31', '2016-04-01 17:35:30'),
(2, 2, 'Acesso 24/7', '24/7 access ', '2016-03-16 15:15:40', '2016-04-01 17:35:45'),
(3, 3, 'Sala privada mobiliada e equipada', 'furnished and equipped private room ', '2016-03-16 15:15:50', '2016-04-01 17:35:51'),
(4, 4, 'Sinalização Digital', 'digital signage', '2016-03-16 15:15:58', '2016-04-01 17:35:59'),
(5, 5, 'Linha telefônica digital privada com caixa postal e acesso remoto', 'dedicated digital telephone line with voicemail and remote access ', '2016-03-16 15:16:22', '2016-04-01 17:36:08'),
(6, 6, 'Equipamento telefônico digital exclusivo', 'digital telephone equipment ', '2016-03-17 16:19:00', '2016-04-01 17:36:21'),
(7, 7, 'Atendimento billíngüe personalizado', 'personalized bilingual service ', '2016-03-17 16:19:27', '2016-04-01 17:36:30'),
(8, 8, 'Redirecionamento de ligações', 'calls redirecting ', '2016-03-17 16:19:36', '2016-04-01 17:36:38'),
(10, 10, '04 horas gratuitas de Salas de Reunião por mês', '04 free monthly hours of meeting room ', '2016-03-17 16:19:54', '2016-04-01 17:36:46'),
(11, 11, 'Acesso ao espaço Infinity Zen', 'access to Infinity Zen ', '2016-03-17 16:20:03', '2016-04-01 17:36:59'),
(12, 12, 'Acesso a Rede de Relacionamento Infinity', 'access to Infinity community', '2016-03-17 16:20:14', '2016-04-01 23:40:44'),
(13, 13, 'Acesso a estrutura de Facilities do Edifício ', 'access to building facilities ', '2016-03-17 16:20:23', '2016-04-01 17:37:21'),
(14, 14, 'Acesso aos Eventos de Networking', 'access to networking events ', '2016-03-17 16:20:30', '2016-04-01 17:37:29'),
(15, 15, 'Mailbox/ Caixa de Correspondências', 'mailbox ', '2016-03-17 16:20:36', '2016-04-01 17:37:39'),
(16, 16, 'Locker/ Armários Individuais*', 'individual cabinets*', '2016-03-17 16:20:45', '2016-04-01 17:38:59'),
(17, 17, 'Estacionamento com manobrista*', 'valet parking *', '2016-03-17 16:20:56', '2016-04-01 17:38:52'),
(18, 18, 'Digital Signage Plan*', 'Digital Signage Plan*', '2016-03-17 16:21:08', '2016-04-01 17:39:15'),
(19, 19, 'Preços Especiais para Salas de Reunião/Showroom/ Treinamento/Auditório', 'special prices for meeting rooms, showrooms, training and auditoriums', '2016-03-17 16:21:19', '2016-04-01 17:38:17'),
(20, 20, 'Organização de Eventos*', 'event organization*', '2016-03-17 16:21:32', '2016-04-01 17:39:23'),
(21, 21, 'Videoconferência*', 'videoconference*', '2016-03-17 16:21:42', '2016-04-01 17:38:46'),
(22, 22, 'Serviços gráficos*', 'printing services*', '2016-03-17 16:21:50', '2016-04-01 17:39:39'),
(23, 23, 'Serviços de secretariado, concierge e mensageiro*', 'secretarial services, concierge and courier*', '2016-03-17 16:21:58', '2016-04-01 17:39:51'),
(25, 25, 'Projeto e Design de Interiores*', 'architectural projects and interior design*', '2016-03-17 16:22:16', '2016-04-01 17:40:05'),
(26, 26, 'Estação de trabalho privada em sala compartilhada', ' private workstation in a shared room', '2016-03-17 16:22:30', '2016-04-01 17:40:14'),
(27, 27, 'Mesa de trabalho/ hot desk rotativa ', 'rotating workstations', '2016-03-17 16:22:40', '2016-04-01 17:40:23'),
(28, 1, 'ISS Reduzido', 'Reduced ISS ', '2016-03-18 00:11:24', '2016-04-01 17:35:38');

-- --------------------------------------------------------

--
-- Estrutura da tabela `comparativo_planos`
--

CREATE TABLE `comparativo_planos` (
  `id` int(10) UNSIGNED NOT NULL,
  `comparativo_id` int(10) UNSIGNED NOT NULL,
  `plano_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `comparativo_planos`
--

INSERT INTO `comparativo_planos` (`id`, `comparativo_id`, `plano_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2016-03-16 15:15:31', '2016-03-16 15:15:31'),
(2, 1, 2, '2016-03-16 15:15:31', '2016-03-16 15:15:31'),
(3, 1, 3, '2016-03-16 15:15:31', '2016-03-16 15:15:31'),
(4, 1, 4, '2016-03-16 15:15:31', '2016-03-16 15:15:31'),
(5, 2, 1, '2016-03-16 15:15:40', '2016-03-16 15:15:40'),
(6, 2, 2, '2016-03-16 15:15:40', '2016-03-16 15:15:40'),
(7, 3, 1, '2016-03-16 15:15:50', '2016-03-16 15:15:50'),
(8, 4, 1, '2016-03-16 15:15:58', '2016-03-16 15:15:58'),
(9, 5, 1, '2016-03-16 15:16:23', '2016-03-16 15:16:23'),
(10, 5, 2, '2016-03-16 15:16:23', '2016-03-16 15:16:23'),
(12, 5, 4, '2016-03-16 15:16:23', '2016-03-16 15:16:23'),
(13, 6, 1, '2016-03-17 16:19:00', '2016-03-17 16:19:00'),
(14, 6, 2, '2016-03-17 16:19:00', '2016-03-17 16:19:00'),
(15, 7, 1, '2016-03-17 16:19:27', '2016-03-17 16:19:27'),
(16, 7, 2, '2016-03-17 16:19:27', '2016-03-17 16:19:27'),
(18, 7, 4, '2016-03-17 16:19:27', '2016-03-17 16:19:27'),
(19, 8, 1, '2016-03-17 16:19:36', '2016-03-17 16:19:36'),
(20, 8, 2, '2016-03-17 16:19:36', '2016-03-17 16:19:36'),
(22, 8, 4, '2016-03-17 16:19:36', '2016-03-17 16:19:36'),
(27, 10, 1, '2016-03-17 16:19:54', '2016-03-17 16:19:54'),
(28, 11, 1, '2016-03-17 16:20:03', '2016-03-17 16:20:03'),
(29, 11, 2, '2016-03-17 16:20:03', '2016-03-17 16:20:03'),
(30, 11, 3, '2016-03-17 16:20:03', '2016-03-17 16:20:03'),
(31, 11, 4, '2016-03-17 16:20:03', '2016-03-17 16:20:03'),
(32, 12, 1, '2016-03-17 16:20:14', '2016-03-17 16:20:14'),
(33, 12, 2, '2016-03-17 16:20:14', '2016-03-17 16:20:14'),
(34, 12, 3, '2016-03-17 16:20:14', '2016-03-17 16:20:14'),
(35, 12, 4, '2016-03-17 16:20:14', '2016-03-17 16:20:14'),
(36, 13, 1, '2016-03-17 16:20:23', '2016-03-17 16:20:23'),
(37, 13, 2, '2016-03-17 16:20:23', '2016-03-17 16:20:23'),
(38, 14, 1, '2016-03-17 16:20:30', '2016-03-17 16:20:30'),
(39, 14, 2, '2016-03-17 16:20:30', '2016-03-17 16:20:30'),
(40, 14, 3, '2016-03-17 16:20:30', '2016-03-17 16:20:30'),
(41, 14, 4, '2016-03-17 16:20:30', '2016-03-17 16:20:30'),
(42, 15, 1, '2016-03-17 16:20:36', '2016-03-17 16:20:36'),
(43, 15, 2, '2016-03-17 16:20:36', '2016-03-17 16:20:36'),
(44, 15, 3, '2016-03-17 16:20:37', '2016-03-17 16:20:37'),
(45, 15, 4, '2016-03-17 16:20:37', '2016-03-17 16:20:37'),
(46, 16, 1, '2016-03-17 16:20:45', '2016-03-17 16:20:45'),
(47, 16, 2, '2016-03-17 16:20:45', '2016-03-17 16:20:45'),
(48, 16, 3, '2016-03-17 16:20:45', '2016-03-17 16:20:45'),
(49, 16, 4, '2016-03-17 16:20:45', '2016-03-17 16:20:45'),
(50, 17, 1, '2016-03-17 16:20:56', '2016-03-17 16:20:56'),
(51, 17, 2, '2016-03-17 16:20:56', '2016-03-17 16:20:56'),
(52, 17, 3, '2016-03-17 16:20:56', '2016-03-17 16:20:56'),
(53, 17, 4, '2016-03-17 16:20:56', '2016-03-17 16:20:56'),
(54, 18, 1, '2016-03-17 16:21:08', '2016-03-17 16:21:08'),
(55, 18, 2, '2016-03-17 16:21:08', '2016-03-17 16:21:08'),
(56, 18, 3, '2016-03-17 16:21:08', '2016-03-17 16:21:08'),
(57, 18, 4, '2016-03-17 16:21:08', '2016-03-17 16:21:08'),
(58, 19, 1, '2016-03-17 16:21:19', '2016-03-17 16:21:19'),
(59, 19, 2, '2016-03-17 16:21:19', '2016-03-17 16:21:19'),
(60, 19, 3, '2016-03-17 16:21:19', '2016-03-17 16:21:19'),
(61, 19, 4, '2016-03-17 16:21:19', '2016-03-17 16:21:19'),
(62, 20, 1, '2016-03-17 16:21:32', '2016-03-17 16:21:32'),
(63, 20, 2, '2016-03-17 16:21:32', '2016-03-17 16:21:32'),
(64, 20, 3, '2016-03-17 16:21:32', '2016-03-17 16:21:32'),
(65, 20, 4, '2016-03-17 16:21:32', '2016-03-17 16:21:32'),
(66, 21, 1, '2016-03-17 16:21:42', '2016-03-17 16:21:42'),
(67, 21, 2, '2016-03-17 16:21:42', '2016-03-17 16:21:42'),
(68, 21, 3, '2016-03-17 16:21:42', '2016-03-17 16:21:42'),
(69, 21, 4, '2016-03-17 16:21:42', '2016-03-17 16:21:42'),
(70, 22, 1, '2016-03-17 16:21:50', '2016-03-17 16:21:50'),
(71, 22, 2, '2016-03-17 16:21:50', '2016-03-17 16:21:50'),
(72, 22, 3, '2016-03-17 16:21:50', '2016-03-17 16:21:50'),
(73, 22, 4, '2016-03-17 16:21:50', '2016-03-17 16:21:50'),
(74, 23, 1, '2016-03-17 16:21:58', '2016-03-17 16:21:58'),
(75, 23, 2, '2016-03-17 16:21:58', '2016-03-17 16:21:58'),
(76, 23, 3, '2016-03-17 16:21:58', '2016-03-17 16:21:58'),
(77, 23, 4, '2016-03-17 16:21:58', '2016-03-17 16:21:58'),
(82, 25, 1, '2016-03-17 16:22:16', '2016-03-17 16:22:16'),
(83, 25, 2, '2016-03-17 16:22:16', '2016-03-17 16:22:16'),
(84, 25, 3, '2016-03-17 16:22:16', '2016-03-17 16:22:16'),
(85, 25, 4, '2016-03-17 16:22:16', '2016-03-17 16:22:16'),
(86, 26, 2, '2016-03-17 16:22:30', '2016-03-17 16:22:30'),
(87, 27, 3, '2016-03-17 16:22:40', '2016-03-17 16:22:40'),
(88, 28, 1, '2016-03-18 00:11:24', '2016-03-18 00:11:24'),
(89, 28, 2, '2016-03-18 00:11:24', '2016-03-18 00:11:24'),
(90, 28, 3, '2016-03-18 00:11:24', '2016-03-18 00:11:24'),
(91, 28, 4, '2016-03-18 00:11:24', '2016-03-18 00:11:24');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `endereco_en` text COLLATE utf8_unicode_ci NOT NULL,
  `codigo_googlemaps` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefone`, `endereco`, `endereco_en`, `codigo_googlemaps`, `created_at`, `updated_at`) VALUES
(1, 'contato@infinitybusiness.com', '+55 11 3036-1800', '<p>Alameda Rio Negro, 503 &middot; 23&ordm;&nbsp;andar</p>\r\n\r\n<p>Edif&iacute;cio Escrit&oacute;rios Rio Negro</p>\r\n\r\n<p>Alphaville &middot; Barueri, SP</p>\r\n\r\n<p>06454-000</p>\r\n', '<p>Alameda Rio Negro, 503, 23rd. Floor</p>\r\n\r\n<p>Edif&iacute;cio Escrit&oacute;rios Rio Negro</p>\r\n\r\n<p>Alphaville, Barueri, SP</p>\r\n\r\n<p>Zip Code 06454-000</p>\r\n', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3658.8695065199336!2d-46.850324285023504!3d-23.50120928471281!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cf0223bf7d7ea3%3A0x5c69e5300371b547!2sAlameda+Rio+Negro%2C+503+-+Alphaville+Industrial%2C+Barueri+-+SP!5e0!3m2!1spt-BR!2sbr!4v1457456321376" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', NULL, '2016-04-01 23:43:31');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contatos_recebidos`
--

INSERT INTO `contatos_recebidos` (`id`, `nome`, `email`, `telefone`, `mensagem`, `lido`, `created_at`, `updated_at`) VALUES
(1, 'tiago correa', 'tcorrea@nuclegem.com.br', '26902606', 'Bom dia, \nGostaria de um orçamento para endereço fiscal e comercial. \nAtenciosamente,\nTiago correa \n', 1, '2016-03-28 20:22:23', '2016-04-01 23:26:27'),
(2, 'teste', 'teste@teste.com.br', '111111111111', 'teste', 0, '2016-03-29 00:18:19', '2016-03-29 00:18:19'),
(3, 'teste', 'teste@teste.com.br', '111111111111', 'teste', 0, '2016-04-06 03:15:56', '2016-04-06 03:15:56'),
(4, 'teste', 'teste@teste.com.br', '111111111111', 'teste', 0, '2016-04-06 03:36:08', '2016-04-06 03:36:08'),
(5, 'teste', 'teste@teste.com.br', '111111111111', 'teste', 0, '2016-04-06 15:28:20', '2016-04-06 15:28:20'),
(6, 'teste', 'teste@teste.com.br', '111111111111', 'teste', 0, '2016-04-06 15:34:24', '2016-04-06 15:34:24'),
(7, 'teste', 'teste@teste.com.br', '111111111111', 'teste', 0, '2016-04-06 16:06:25', '2016-04-06 16:06:25'),
(8, 'teste', 'teste@teste.com.br', '111111111111', 'teste', 0, '2016-04-06 16:34:46', '2016-04-06 16:34:46'),
(9, 'teste', 'teste@teste.com.br', '111111111111', 'teste', 0, '2016-04-08 06:35:51', '2016-04-08 06:35:51'),
(10, 'teste', 'teste@teste.com.br', '111111111111', 'teste', 0, '2016-04-08 23:37:37', '2016-04-08 23:37:37'),
(11, 'teste', 'teste@teste.com.br', '111111111111', 'teste', 0, '2016-04-08 23:39:26', '2016-04-08 23:39:26'),
(12, 'teste', 'teste@teste.com.br', '111111111111', 'teste', 0, '2016-04-11 18:29:44', '2016-04-11 18:29:44'),
(13, 'teste', 'teste@teste.com.br', '111111111111', 'teste', 0, '2016-04-12 15:38:06', '2016-04-12 15:38:06'),
(14, 'teste', 'teste@teste.com.br', '111111111111', 'teste', 0, '2016-04-12 16:15:08', '2016-04-12 16:15:08'),
(15, 'teste', 'teste@teste.com.br', '111111111111', 'teste', 0, '2016-04-12 16:30:55', '2016-04-12 16:30:55'),
(16, 'teste', 'teste@teste.com.br', '111111111111', 'teste', 0, '2016-04-12 16:33:48', '2016-04-12 16:33:48'),
(17, 'teste', 'teste@teste.com.br', '111111111111', 'teste evento', 0, '2016-04-12 16:58:04', '2016-04-12 16:58:04'),
(18, 'teste', 'teste@teste.com.br', '111111111111', 'teste', 0, '2016-04-12 17:10:08', '2016-04-12 17:10:08'),
(19, 'teste', 'teste@teste.com.br', '111111111111', 'teste', 0, '2016-04-12 17:24:30', '2016-04-12 17:24:30'),
(20, 'Anderson Ribeiro', 'arsribeirosolut@gmail.com', '11941234022', 'proposta  de  escritorio virtual e sala para  aluguel  !', 0, '2016-04-13 15:23:40', '2016-04-13 15:23:40'),
(21, 'Samuel Rodrigues Ayres', 'sam.rodrigues.ayres@gmail.com', '+5511941551845', 'Bom dia\n\nGostaria de saber se vocês efetuam o serviço de locação apenas para endereço fiscal junto à prefeitura de Barueri.\n\nObrigado, att\n\nSamuel', 0, '2016-04-13 17:47:19', '2016-04-13 17:47:19'),
(22, 'andrea', 'motofelixexpress@gmail.com', '2989-8580', 'Moto Felix Express\nAv. Ede - Nº: 705, Zona Norte - SP\nTEL: 2989-8580 ou 2951-5065\n \nAssunto : Proposta de Prestação de serviços Motoboy e veiculo utilitário.\n \nCaros Senhores,\n \n A Moto Felix Express é uma Empresa especializada em prestação de serviços com motoboy e veiculo utilitário, com 15 anos de experiência, nos capacitando para lhes prestar um atendimento personalizado.\n \nEstamos no mercado com a finalidade de lhe oferecer serviços com qualidade, rapidez e confiança.\n \nNossos colaboradores estão aptos a executar serviços junto a Bancos, cartórios, repartições públicas e comércios em geral.\n \nNossos serviços são pré fixados, por Pontos, valor de R$13,00 o ponto, evitando surpresas indesejáveis, os serviços poderão ser esporádicos com pagamento no ato do mesmo ou Faturado mensalmente para empresas após prévio e fácil cadastro.\n \nNa certeza de lhe oferecermos organização, responsabilidade, segurança e supervisão de nossos serviços, nos colocamos a sua disposição para quaisquer dúvidas.\n \nLigue já e faça uma cotação da sua necessidade e saiba quanto ira pagar antes, atendemos toda a capital, litoral e interior de São Paulo.  \n \n                                                                                             \n   Cadastre sua empresa.        \n \nÉ rápido e grátis.\n \n                    LIGUE JÁ !!!', 0, '2016-04-14 17:50:35', '2016-04-14 17:50:35'),
(23, 'HELLEN LIMA ', 'arquidecor.projetos@outlook.com', '1144086001', 'Boa tarde por gentileza enviar proposta com os custo fixos e taxa dos serviços variáveis (sala de reunião, estacionamento, taxas de ligações.)\n\nFico no aguardo \n\nAtenciosamente,\n\nHellen Lima ', 0, '2016-04-14 20:55:40', '2016-04-14 20:55:40'),
(24, 'Marcela Buttazzi', 'coachmbuttazzi@gmail.com', '11 99201-7997', 'Olá gostaria de agendar sala para 2 posições ou sala de reunião por hora, gostaria também de saber os valores por hora. \n\nAguardo retorno.\n\nAbraços, \n\nMarcela Buttazzi ', 0, '2016-04-14 22:38:28', '2016-04-14 22:38:28'),
(25, 'Brunno', 'bplzaca@hotmail.com', '11 976187045', 'Estou tentando ligar aí insistentemente mas a ligacao toda hora cai. ', 0, '2016-04-15 20:37:03', '2016-04-15 20:37:03'),
(26, 'Silvana', 'silvanaparecida@gmail.com', '953674368', 'Bom dia,\n\nGostaria de receber por e-mail uma proposta para endereço comercial e atendimento telefônico.\n\nObrigada', 0, '2016-04-19 17:12:21', '2016-04-19 17:12:21'),
(27, 'Eduardo beirouti de miranda Roque', 'ebmroque77@gmail.com', '11971476508', 'Gostaria de uma cotação. Escritório mobiliado com 3 profissionais. Com todos serviços inclusos.', 0, '2016-04-19 17:53:03', '2016-04-19 17:53:03'),
(28, 'Eduardo beirouti de miranda Roque', 'ebmroque77@gmail.com', '11971476508', 'Gostaria de uma cotação. Escritório mobiliado com 3 profissionais. Com todos serviços inclusos.', 0, '2016-04-19 17:53:03', '2016-04-19 17:53:03'),
(29, 'Eduardo beirouti de miranda Roque', 'ebmroque77@gmail.com', '11971476508', 'Gostaria de uma cotação. Escritório mobiliado com 3 profissionais. Com todos serviços inclusos.', 0, '2016-04-19 17:53:05', '2016-04-19 17:53:05'),
(30, 'Eduardo beirouti de miranda Roque', 'ebmroque77@gmail.com', '11971476508', 'Gostaria de uma cotação. Escritório mobiliado com 3 profissionais. Com todos serviços inclusos.', 0, '2016-04-19 17:53:05', '2016-04-19 17:53:05'),
(31, 'Eduardo beirouti de miranda Roque', 'ebmroque77@gmail.com', '11971476508', 'Gostaria de uma cotação. Escritório mobiliado com 3 profissionais. Com todos serviços inclusos.', 0, '2016-04-19 17:53:05', '2016-04-19 17:53:05'),
(32, 'Gilberto Georg', 'gilbertogeorg@gmail.com', '1132895241', 'Prezados, gostaria de confirmar algumas informações:\n\na - O plano VBP inclui o endereço comercial e fiscal?\n\nb - Existe alguma taxa de adesão?\n\nc - O que é necessário para a locação?\n\n\nObrigado.\n\n', 0, '2016-04-20 21:55:56', '2016-04-20 21:55:56'),
(33, 'Christina Romeo Garcia', 'christina.romeo@hotmail.com', '11983522113', 'Olá, sou Psicóloga e Coaching e queria uma sala individual para atender no período da noite (das 19h as 22h). Vocês teriam alguma estrutura que me atendesse dessa forma?\nObrigada', 0, '2016-04-24 01:52:43', '2016-04-24 01:52:43'),
(34, 'Dra Rita Boscolo Bonnard ', 'psicanalize@hotmail.com', '11989307494', 'Procuro uma sala pequena com baneiro e sala de esprra , pois sou psicóloga e pretendo abrir meu consultório. Já atendo desde 1999.', 0, '2016-04-25 18:08:30', '2016-04-25 18:08:30'),
(35, 'Paulo Sergio Dalpicolo', 'vendas@edlei.com.br', '11-981027311', 'Boa tarde Srs!\n\nEu visitava vocês através da empresa JEA, mas agora estou em outra empresa do segmento de portas automáticas.\n\nTomei a liberdade e estou encaminhando uma apresentação dos produtos e da empresa. Caso saiba de alguém que necessite ou principalmente vocês mesmo, estou à disposição.\n\nCerto de sua compreensão.\n\nFico no aguardo para quaisquer necessidade.\n\nAbraços e Sucesso.\n\nAtt,\n\nPaulo Sergio Dalpicolo\nVendedor Técnico\n11-8102-7311\n', 0, '2016-04-25 23:26:34', '2016-04-25 23:26:34'),
(36, 'vanessa de angelis', 'tfarcondicionado2016@gmail.com', '11947083155', 'Boa Noite,\n\nVenho por meio deste divulgar a nossa empresa e os serviços que prestamos;\nEstamos trabalhando com instalação, manutenção e higienização de ar condicionado.\nOferecemos ótimos preços e formas de pagamento.\nMarque uma visita técnica com os nossos representantes sem compromisso.\n\nQualquer dúvida,esclarecimentos e negociação estamos á disposição,\nAtenciosamente,\n\n\nTF Ar Condicionado(Vanessa)\n\n', 0, '2016-04-26 05:08:59', '2016-04-26 05:08:59'),
(37, 'Julie Campanholi', 'contato@juliecampanholi.com.br', '11981445727', 'Olá, gostaria de receber mais informações sobre o PBP Premium Business Plan e o  SBP Shared Business Plan.\n\nAguardo, Julie.', 0, '2016-04-27 03:20:19', '2016-04-27 03:20:19'),
(38, 'Maria do socorro Amorim d', 'socorro.help63@gmail.com', '984409006', 'Gostaria de saber como faço para assecar meu loguim no escritório virtual. Pois sempre que eu tento entrar aparece senha ou loguim errado. Tento enviar o pedido de nova senha ,asseso meu email não recebo nada.tá parecendo sacanagem.se essa empresa e uma empresa séria espero que me dêem uma solução .grata fico aguardando. ..\n', 0, '2016-04-29 03:57:06', '2016-04-29 03:57:06'),
(39, 'unimed', 'rosali.ferreira@segurosunimed.com.br', '1132659995', 'Prezado,\nBom dia,\nEstamos buscando uma sala comercial para locação de 85 lugares para trabalhar como escritório.\n\nLocalização: Região da Av. Paulista de preferência próximo Alameda Ministro Rocha de Azevedo, x Alameda Santos – Cerqueira Cesar\n\nMobiliário: enviar proposta com mesas e sem mesas\n\nInclui: equipe especializada para rede, elétrica, lógica, telefone\n\nPeríodo: 50 dias (data a confirmar)\n\nEnviar proposta + documentação para homologação (contrato social, alvará de funcionamento e preencher ficha cadastral modelo anexo).\n\n', 0, '2016-05-02 22:31:55', '2016-05-02 22:31:55'),
(40, 'unimed', 'rosali.ferreira@segurosunimed.com.br', '1132659995', 'Prezado,\nBom dia,\nEstamos buscando uma sala comercial para locação de 85 lugares para trabalhar como escritório.\n\nLocalização: Região da Av. Paulista de preferência próximo Alameda Ministro Rocha de Azevedo, x Alameda Santos – Cerqueira Cesar\n\nMobiliário: enviar proposta com mesas e sem mesas\n\nHorário Comercial: 8 as 18h (segunda a sexta)\n\nInclui: equipe especializada para rede, elétrica, lógica, telefone\n\nPeríodo: 50 dias (data a confirmar)\n\nEnviar proposta + documentação para homologação (contrato social, alvará de funcionamento e preencher ficha cadastral modelo anexo).\n\n\n', 0, '2016-05-03 17:30:30', '2016-05-03 17:30:30'),
(41, 'Maya Lima', 'maya.cyrela@gmail.com', '20212312', 'Bom dia, trabalho com beleza, estética e moda feminina.\nGostaria de saber se existe algum plano desses que se encaixa para minha empresa?', 0, '2016-05-03 18:28:04', '2016-05-03 18:28:04'),
(42, 'Francisco', 'francisco.thaga@gmail.com', '1198505-1858', 'Boa tarde,\nConsiderando espaço para 12 profissionais, com sala para acomodação para uso de segunda a sexta-feira, gostaria de saber valores?', 0, '2016-05-03 22:46:44', '2016-05-03 22:46:44'),
(43, 'Otávio', 'otavio@dryoffices.com', '011 4163 - 2804', 'Olá bom dia ,tudo bom?\n\n\nSomos a dry-offices,uma Empresa especializada em Projetos,Preparações e Reformas de salas comerciais,tanto p/ uso particular como para simples locação!\n\nCom o proposito de poder ajuda-los em eventuais reformas envio uma Apresentação especificando os serviços que executamos para o seu conhecimento.\n\n\nObrigado', 0, '2016-05-04 15:30:38', '2016-05-04 15:30:38'),
(44, 'Bruna Moreira Alves', 'bruna.alves@daysanbrasil.com', '(11) 971451607', 'Prezados Boa tarde!\n\nPor gentileza, gostaria de um orçamento referente ao plano Premium Business.\n\nDesde já agradeço.', 0, '2016-05-04 19:32:38', '2016-05-04 19:32:38'),
(45, 'Lynn Lombardo', 'lynn.lombardo@workplacesolutions.com', '7326723402', 'I need pricing for 20-30 people starting May 18th for 1 year.    I am a Broker with this referral.  What is your Broker commission policy?', 0, '2016-05-05 04:38:54', '2016-05-05 04:38:54'),
(46, 'Mariane Alves', 'mariane@jjvideos.com.br', '(11)9607-61042', 'Bom dia, tudo bem?\n\nGostaria de apresentar a JJ Vídeos - Produtora de vídeos explicativos animados.\n\nNosso objetivo é fazer com que sua empresa alcance novos clientes através de vídeos publicitários.\nVídeos Animados tem sido a melhor maneira para divulgar sua marca, produtos e serviços!\n\nAssista ao último vídeo que fizemos:\nhttps://www.youtube.com/watch?v=k8KmUkBbSxY\n\n\nAcesse nosso site e saiba mais!\nhttp://jjvideos.com.br\n\nEspero que goste, e aguardo seu contato em breve!\n\nAtenciosamente,\n', 0, '2016-05-05 18:58:57', '2016-05-05 18:58:57'),
(47, 'Natasha Magano', 'natasha.magano@cleaner.com.br', '1127233908', 'Olá! \nSou a Natasha, representante de vendas da SALES DISTRIBUIDORA, somos uma empresa especializada em vendas de material descartável, higiene e limpeza, localizados na cidade de São Paulo. \nO motivo do meu contato é oferecer os nossos serviços. \nTemos diversas promoções e excelente formas de pagamento, entre elas a facilidade da compra com Cartão de Crédito. \nAlém da forma rápida e eficiente de entrega. \nMe retorne, faça um orçamento sem compromisso e conheça o nosso trabalho.\nTemos promoções diárias, venha conferir! \n\nObrigada! ', 0, '2016-05-05 22:37:38', '2016-05-05 22:37:38'),
(48, 'LUCAS JUNCAL', 'vendas02@tobiasviagens.com.br', '14 32040499', 'Boa tarde.\n\nSolicito cotação para sala de evento dia 13/05 para 250 pessoas no período das 13H as 19H.\n\nCom opções de coffe breack.\n\nSerá utilizado data show.\n\nPeço por favor a localização ex- zona leste, zone oeste...\n\n\nFico no agurdo.\n\nObrigado.\n\n', 0, '2016-05-06 00:19:16', '2016-05-06 00:19:16'),
(49, 'Murilo Ronchesel', 'mr@arkus.com.br', '1436264516', 'Prezados, boa tarde!\nGostaria de saber os valores de planos de escritório virtual com endereço fiscal.\nDesde já agradeço.', 0, '2016-05-07 23:49:57', '2016-05-07 23:49:57'),
(50, 'Sandra jose valentim ', 'sandra@motomlitar.com.br', '11-981024532', 'Bom dia,\nEstou construindo um espaço com a idéia de usa-lo como coworking! Estou precisando de uma visita de vocês. \nAtt.,\nSandra ', 0, '2016-05-08 13:00:05', '2016-05-08 13:00:05'),
(51, 'gabrie', 'gabrielasoares@ellonetwork.com.br', '11 983364539', 'gostaria de saber valores de planos virtuais,salas de reuniões e estações de trabalho', 0, '2016-05-09 18:08:10', '2016-05-09 18:08:10'),
(52, 'Caroline', 'caroline.siqueira@blueticket.com.br', '48 40529003', 'Boa tarde!\n\nGostaria de orçamento para escritório virtual em SP. Meu interesse é apenas em endereço para uso em site e material de comunicação, recebimento de correspondências e para eventual uso de espaço para reunião.\n\nVocê possuem esse serviço? Pois não localizei no site de vocês.\n\nMuito obrigada.', 0, '2016-05-09 19:10:24', '2016-05-09 19:10:24'),
(53, 'Marcelo Camilo', 'marcelo.camilo@promon.com.br', '11- 5213 4469', 'Prezados, \nPeço por gentileza, envio de proposta comercial para ocupação entre 20 a 30 postos de trabalho em Alphaville.\nFavor informar sobre as condições gerais da contratação: taxa de setup, tempo mínimo de contrato, serviços inclusos, vagas de estacionamento, salas de reunião etc.\n\nDesde já agradeço e aguardo por informações.\n\nObrigado,\nMarcelo Camilo', 0, '2016-05-10 17:57:58', '2016-05-10 17:57:58'),
(54, 'Jonatas', 'jonatas@jjvideos.com.br', '11960761042', 'Boa tarde, tudo bem?\n\nGostaria de apresentar a JJ Vídeos - Produtora de vídeos explicativos animados.\n\nNosso objetivo é fazer com que sua empresa alcance novos clientes através de vídeos publicitários.\nVídeos Animados tem sido a melhor maneira para divulgar sua marca, produtos e serviços!\n\nAssista ao exemplo de vídeo que fazemos:\nhttps://www.youtube.com/watch?v=k8KmUkBbSxY\n\n\nAcesse nosso site e saiba mais!\nhttp://jjvideos.com.br\n\nEspero que goste, e aguardo seu contato em breve!\n\nAtenciosamente,', 0, '2016-05-10 22:02:13', '2016-05-10 22:02:13'),
(55, 'bianca francine radtke', 'biancafrancine.0803@gmail.com', '11 984228000', 'Boa tarde\nVocês alugam sala de 30 metros (mais ou menos), por 2 horas?\nIntenção - zona sul de SP.\nAbc', 0, '2016-05-11 21:43:53', '2016-05-11 21:43:53'),
(56, 'Conserv Ar Condicionado', 'contato@conservarcondicionado.com.br', '11 22560898', 'Ola,\n\nNão espere o verão chegar para limpar seu ar condicionado! No verão as limpezas custam até 60% mais caro do que no inverno e a fila de espera pode chegar a 30 dias.\n\nEstamos prontos para lhe atender enviando nossa equipe técnica para limpeza do seu ar condicionado in-loco. Não perca tempo, ligue agora mesmo ou solicite orçamento sem compromisso!\n\nAtenciosamente\n\nAna Soares-Depto-financeiro\n(11)2256-0898-98337-1065(whatsap)\n\n\n\nIMPORTÂNCIA DA MANUTENÇÃO PREVENTIVA DE AR CONDICIONADO\n\n\nIndependente do sistema que você tenha em sua empresa, seja ele um split,\nself, chiller ou uma vrf, os critérios são os mesmos. Prevenir para\nevitar a parada repentina inesperada. Em primeiro lugar, porque dependendo\ndo ambiente, podemos ter uma redução na produção de um setor,\ndepartamento ou até mesmo de uma região. Em segundo, porque significa um\naumento de custos operacionais com a manutenção de ar condicionado, aonde\num único reparo pode custar o equivalente a 01 ano de contrato. Isso sem\ncontar com a redução do tempo de vida útil do equipamento, que pode ser\nreduzida em até 70%, dependendo a frequência de uso do sistema.\n\nManutenção Mensal\n\nTer um contrato de manutenção para ar condicionado aumenta a eficiência\nde energia de seu equipamento, prevenindo falhas de funcionamento e danos\nno sistema, assegurando maior vida útil ao equipamento.Com um contrato de\nmanutenção preventiva seu equipamento receberá inspeções periódicas\npara otimizar o desempenho, além de beneficiar os ocupantes do ambiente\nclimatizado, prevenindo-os contra enfermidades ocasionadas por bactérias\nnocivas ao ambiente sujo.\n\nVantagens de manter um Contrato de Manutenção Preventiva para Ar\nCondicionado:\n\n• Aumento do tempo de parada dos equipamentos, reduzindo efetivamente os\ncustos com energia elétrica\n• Equilíbrio do sistema de refrigeração, favorecendo o prolongamento\nda vida útil do equipamento\n• Limpeza efetiva dos filtros evaporadores, obtendo ar puro livre de\nbactérias responsáveis por doenças respiratórias\n• Melhorias na qualidade do ar interno\n• Melhor funcionamento do compressor, aumentando sua vida útil\n• Pronto atendimento na ocorrência de possíveis problemas\n• Redução no custo final de utilização e manutenção\n• Redução de gastos com a troca de peças, panes e quebras dos\nequipamentos\n• Utilização de mão de obra técnica especializada\n• Utilização de ferramentas adequadas à execução dos serviços\n\nR$ 200,00\n\nConheça nosso site:\nhttp://www.conservarcondicionado.com.br', 0, '2016-05-13 16:18:27', '2016-05-13 16:18:27'),
(57, 'Rogerio Rampinelli Passotto', 'rogerio.passotto@hotmail.com', '(11) 979596690', 'Estamos em um startup de um site de modas e precisamos de um local para reuniões e endereço físico. ', 0, '2016-05-13 18:38:59', '2016-05-13 18:38:59'),
(58, 'cindi helen', 'contato@bbmarketing.com.br', '4830451563', 'Olá, como vai?\n\nEu sou a Cindi Helen, da agência Blueberry e vi no seu anúncio do Google um potencial enorme de retorno e com algumas adequações, você pode fazer de 3 a 10 vezes mais vendas, sabia?\n\nDeixa eu te contar como...\n\nQuando você faz um anúncio, o Google dá algumas indicações pra te ajudar, mas elas tem um custo geralmente maior e não acertam direitinho o seu público alvo. Então o anúncio funciona, mas não tão bem e não tão barato quanto poderia.\n\nNós da Blueberry podemos te dar melhores resultados porque temos uma equipe de especialistas certificados em Google Adwords, e já ajudamos nossos clientes a faturar mais de 100 milhões de reais nos últimos 5 anos.\n\nIsso mesmo, nossos clientes venderam mais de 100 milhões de reais nos últimos anos sem que eles mesmos precisassem ficar quebrando a cabeça com anúncios, testes e análises. E eu ficaria honrado em poder fazer isso por você, aumentar de 3 a 10 vezes o seu retorno financeiro, aumentando as suas vendas e deixando você focado no seu negócio!\n\nSó que você não pode decidir isso sem nos conhecer, certo? Por isso o meu gerente, Robson, liberou algumas consultorias GRATUITAS para empresários com o perfil da sua empresa. Assim podemos dar uma olhada em suas campanhas e analisar o potencial de melhorias. O que acha?\n\nEntão se você quiser que eu te dê essa consultoria, com o valor de R$250,00 a hora SEM CUSTO ALGUM, é só me responder este e-mail. Tenho poucas vagas e consigo esperar a sua resposta por 24 horas.\n\nDeixe essa parte técnica dos anúncios Google com a gente e ganhe tempo pra expandir o seu negócio ;-)\n\nFico no seu aguardo.\n\nhttp://bbmarketing.com.br\ncontato@bbmarketing.com.br\n\nPs.: Temos centenas de cases de sucesso, clientes como como Supermercado Bistek, Unisul, Fotógrafa Larissa Ronchi, Bike Point que diminuíram muito os seus custos de divulgação no Google ao mesmo tempo que aumentaram incrivelmente as suas vendas. Então, vamos começar? Aguardo o seu contato...\n', 0, '2016-05-13 20:49:11', '2016-05-13 20:49:11'),
(59, 'Ana Cristina', 'ana.crisina770@gmail.com', '3287', 'Olá pessoal!\nVisitei o vosso site e parabenizo pela brilhante apresentação.\nAproveito e faço um convite para visitarem também o nosso site.\n\nEstamos lançando um incrível produto que acaba com os cabelos brancos.\nTrata-se de um excelente cosmético que por sua eficiência a cada dia\nConquista novos admiradores.\n\nEsperamos a sua visita:\nwww.amazoniaprodutos.com.br\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n#1\n', 0, '2016-05-14 04:49:06', '2016-05-14 04:49:06'),
(60, 'vanessa de angelis', 'tfarcondicionado2016@gmail.com', '1139214158', 'Boa Noite,\n\nVenho por meio deste divulgar a nossa empresa e os serviços que prestamos;\nEstamos trabalhando com instalação, manutenção e higienização de ar condicionado.\nOferecemos ótimos preços e formas de pagamento.\nMarque uma visita técnica com os nossos representantes sem compromisso.\n​\nQualquer dúvida,esclarecimentos e negociação estamos á disposição,\nAtenciosamente,\n\nTF Ar Condicionado\nSecretária Vanessa', 0, '2016-05-14 07:52:16', '2016-05-14 07:52:16'),
(61, 'Alessandro Gastão', 'alessandrogastao@theopportunity.com.br', '11 982111151', 'Gostaria de ter acesso aos valores.', 0, '2016-05-15 20:26:14', '2016-05-15 20:26:14'),
(62, 'Susana (Apresentação Studio SSO Arquitetura)', 'susana@studiosso.com.br', '(11) 9 7421-7666', 'Apresento o STUDIO SSO, escritório de arquitetura, interiores e fotografia.\nPoderiam por gentileza me informar um e-mail ou telefone direto com a área de contratação, para possamos divulgar nosso trabalho?\n\nSegue nossa fanpage: \nhttps://www.facebook.com/Studio-SSO-1503892689935935/\n\nDesde já, \nAgradeço a atenção.\n\nMuito Obrigada,\nSusana Sodré de Oliveira\nStudio SSO\n\n\n', 0, '2016-05-16 16:37:36', '2016-05-16 16:37:36'),
(63, 'Carolina TOmita', 'carolinatomita@hotmail.com', '11 94171-2939', 'Olá, o meu contato é para indicação de um ponto comercial.\nSou proprietária de imóvel localizado em uma via bastante movimentada na Zona Sul, a Av.Fagundes Filho, 427. Esta avenida cruza a Av.Jabaquara até a Av.Ricardo Jafet, ficando  próximo ao metrô São Judas. \nO imóvel tem 450 m2, um sobrado comercial com subsolo (tem um anfiteatro), térreo e piso superior. \n\nHavendo interesse, fico à disposição. \nObrigada!', 0, '2016-05-16 17:06:44', '2016-05-16 17:06:44'),
(64, 'Osvaldo Barbosa', 'playoffmusicproducoes@gmail.com', '(62) 82009863', 'Bom dia, qual valor do plano PBP e o que ele contem ? o numero de telefone será exclusivo da minha empresa, poderia divulgar no cartão endereço e numero de telefone ? receberei endereço fiscal? atendimento desse telefone será em horário comercial e personalizado ? e em caso de correspondência e recados como são repassados ?', 0, '2016-05-16 17:50:12', '2016-05-16 17:50:12'),
(65, 'Fábio Zolli Junior', 'fabio@estudioj2g.com.br', '11971133145', 'Olá bom dia!\n\nSou arquiteto e preciso de um espaço para 2 pessoas, quais as opções?\n\nAtt.', 0, '2016-05-20 16:41:33', '2016-05-20 16:41:33'),
(66, 'Carlos', 'karllos@mackenzista.com.br', '11 997 933 406', 'Bom dia!\n\nDúvidas:\n\n\nGostaria de saber se é permitido o uso da estrutura no plano básico de $1000 além do horário comercial e também aos finais de semana e feriados\n\nHá alguma promoção no momento para o plano mensal além do informado?\n\nHá disponibilidade de notebook ou desktop para utilização?\n\nÉ possível compartilhar a mesma mesa com uma segunda pessoa?\n \n\nNo Escritório Virtual por $400 disponibiliza quantas utilizações da sala de reunião?\n', 0, '2016-05-20 18:39:06', '2016-05-20 18:39:06'),
(67, 'walter caetano', 'welly.caetano1@hotmail.com', '11 959722893', 'bom dia,a empresa W.corporate gostaria de prestar  serviço de instalaçao de audio e video,como projetores,telas,tv e etc. obrigado', 0, '2016-05-21 23:39:51', '2016-05-21 23:39:51'),
(68, 'Joelma Souza dos Santos', 'Joelmabeauty@hotmail.com', '(11)962976602', 'Boa tarde.\nGostaria de mais informações.\nÉ uma espécie de locação do espaço?\nPor que eu trabalho na área de beleza e estética há um bom tempo, e achei interessante.\nAguardo resposta.', 0, '2016-05-23 19:34:34', '2016-05-23 19:34:34'),
(69, 'FAbio Vernalha', 'fabio.vernalha@overmalls.com', '(11) 99921-1444', 'Bom dia,\nGostaria de saber se possuem plano apenas com o endereço fiscal e comercial. E qual o valor mensal?\n\nTenha uma startup de prestação de serviços.\n\nAtenciosamente', 0, '2016-05-24 16:13:20', '2016-05-24 16:13:20'),
(70, 'João James P. Mello', 'james_mello@atlantico.com.br', '(11)99951-1703', 'A empresa que atuo esta precisando de um espaço para umas 7 pessoas por um período de +- 6 meses.\nGostaria de saber se vcs tem e qual é a estrutura que voces oferecem.', 0, '2016-05-27 18:40:31', '2016-05-27 18:40:31'),
(71, 'João James P. Mello', 'james_mello@atlantico.com.br', '(11)99951-1703', 'A empresa que atuo esta precisando de um espaço para umas 7 pessoas por um período de +- 6 meses.\nGostaria de saber se vcs tem e qual é a estrutura que voces oferecem.', 0, '2016-05-27 18:40:31', '2016-05-27 18:40:31'),
(72, 'Gabriela', 'gabi@atlantico.com.br', '85999170198', 'Gostaria de saber se vocês tem disponibilidade de sala exclusiva com 20 estações de trabalho. \nSe houver a disponibilidade, por favor, informar o valor e os serviços oferecidos. \n', 0, '2016-06-07 21:26:34', '2016-06-07 21:26:34'),
(73, 'Harold fontoura', 'HAROLD_FONTOURA@HOTMAIL.COM', '11974769505', 'Aguardando contato para locação', 0, '2016-06-08 17:48:06', '2016-06-08 17:48:06'),
(74, 'Nido Meireles ', 'nidomeireles@revistanovafamilia.com.br', '11 99654-8519', 'Parabéns a equipe! Em breve estarei para apresentar um belo projeto para a região !!\nUm abraço ', 0, '2016-06-09 13:10:26', '2016-06-09 13:10:26'),
(75, 'Patricia Melo', 'viagenspelomundo16@gmail.com', '11959078570', 'Prezados\n\nGostaria de informações para locação de sala mensal', 0, '2016-06-10 22:51:24', '2016-06-10 22:51:24'),
(76, 'Fabricio', 'fabricio.stadler@hafele.com.br', '41 30348150', 'Boa tarde,\n\nPrecisamos dos seguintes serviços:\n- endereço comercial e fiscal;\n- escritório, sala de reunião, auditório, sob demanda;\n- recepção de correspondências e outras remessas de pequeno porte - material promocional, pequenas amostras etc.\n\nSolicito por gentileza o envio das valores desses serviços.\nMais uma dúvida: o edifício conta com estacionamento? \n\nAtenciosamente,\nFabricio Stadler', 0, '2016-06-14 21:11:15', '2016-06-14 21:11:15'),
(77, 'Aline Nantes', 'aline.nantes@vivatem.com.br', '11 998425582', 'Olá, Boa tarde!!\n\nGostaria de entender um pouquinho mais o espaço de vocês.\n\nTenho uma necessidade hoje de ter um local para receber correspondências e ligações e além disso precisaria de uma sala para reunião apenas 1x na semana.\n\nAguardo contato para entender melhor!\nMuito Obrigada!\n\nAbs\n\nAline Nantes\n11 99842-5582', 0, '2016-06-15 21:35:50', '2016-06-15 21:35:50'),
(78, 'FERNANDO', 'fernando_vancetti@yahoo.com.br', '11963912239', 'Sou advogado e atendo clientes uma vez por semana por no máximo 1 hora, precisaria de até 5 horas por mês ', 0, '2016-06-18 20:12:50', '2016-06-18 20:12:50'),
(79, 'Fabiano Hirooka', 'fabiano.hirooka@gmail.com', '11 996530434', 'Por favor preciso de cotação para:\n\n- endereco fiscal para 4 CNPJs\n\n- mesa de trabalho para 4 pessoas 1x semana\n\n- receber telefonemas e correspondencias', 0, '2016-06-20 17:46:34', '2016-06-20 17:46:34'),
(80, 'Fabiano Hirooka', 'fabiano.hirooka@vivatem.com.br', '11 996530434', 'Por favor preciso de cotação para:\n\n- endereco fiscal para 4 CNPJs\n\n- mesa de trabalho para 4 pessoas 1x semana\n\n- receber telefonemas e correspondencias', 0, '2016-06-20 17:46:59', '2016-06-20 17:46:59'),
(81, 'Flavio Chirichella', 'flaviochiri@gmail.com', '11 97311-1230', 'Tenho interesse em ter um escritório virtual em Alphaville e quero receber proposta para tal.\nNo aguardo,\nFlavio\n', 0, '2016-06-24 17:43:53', '2016-06-24 17:43:53'),
(82, 'mria de lourdes erminia santos', 'lourdes@hu.usp.br', '58746125', 'gostaria de o bter meu hider\n', 0, '2016-06-24 22:34:22', '2016-06-24 22:34:22'),
(83, 'William Maximiano', 'wmaximiano@imconsulting.com.br', '(11) 98626-8453', 'Prezados,\n\nPreciso de um endereço para minha empresa com as seguintes exigências:\n\nA) CÓPIA DO LANÇAMENTO DO IMPOSTO PREDIAL E TERRITORIAL URBANO - IPTU DO ESTABELECIMENTO DA PARTE ONDE CONSTA A METRAGEM DO TERRENO E A  ÁREA CONSTRUIDA, REF. AO EXERCÍCIO MAIS RECENTE;\nB) COPIA DO CONTRATO DE LOCAÇÃO, SE FOR O CASO, COM FIRMA RECONHECIDA DOS SIGNATÁRIOS;\nC) CÓPIA DAS FATURAS DE PELO MENOS 1 (UM) TELEFONE DOS ÚLTIMOS 6 (SEIS) MESES (11/12/2015 e 01/02/03/04/2016) EM QUE CONSTE O ENDEREÇO DO ESTABELECIMENTO;\nD) COPIA DA CONTA DE ENERGIA ELÉTRICA EM QUE COSNTE O ENDEREÇO DO ESTABELECIMENTO mês 12/2015 e 05/2016 parte do Histórico do consumo;\nE) 3 (TRÊS) FOTOGRAFIAS FRONTAL E DETALHE DO NUMERO e DA AREA DE TRABALHO ( MESA DO COMPUTADOR)\n\nAguardo orçamento,\n', 0, '2016-06-25 02:51:52', '2016-06-25 02:51:52'),
(84, 'Ludimila', 'ludimila.vendasonline@gmail.com', '11942985001', 'Quais os valores de salas e coffee break para reunião com vídeo conferência', 0, '2016-06-27 16:11:05', '2016-06-27 16:11:05'),
(85, 'Silvia Angelo', 'sangelo@zagreoengenharia.com', '11-996435535', 'Gostaria de receber as condições , preços e etc, para a contratação de VBP\n\nGrata', 0, '2016-06-27 22:48:35', '2016-06-27 22:48:35'),
(86, 'Marcia Catarina', 'mormai.corretora@outlook.com', '', 'Quanto fica a sala para 03 pessoas, consigo imprimir e fazer cópias de documentos no local? Qual o custo?', 0, '2016-06-29 17:02:15', '2016-06-29 17:02:15'),
(87, 'Maykon Batista da Silva', 'maykonbts@gmail.com', '', 'Bom dia,\n\nEstou em um projeto juntamente a dois sócios, com isso estamos pesquisando áreas de coworking e gostaríamos de conhecer seu espaço e propostas.\n\nAtt,\nMBS', 0, '2016-06-30 16:53:09', '2016-06-30 16:53:09'),
(88, 'Roberto Aliberti', 'roberto.aliberti@newmarkgrubb.com.br', '2737-3130', 'Prezados,\n\nTemos um cliente que provavelmente necessitará de espaço para 15 a 25 pessoas, durante 6 meses, a partir de outubro ou novembro.\nSe possível, favor nos informar qual seria o custo por pessoa e quaisquer outros custos adicionais, tais como a hora de sala de reunião, armários, gaveteiros extras, etc.\n\nFicamos no aguardo e, desde já, agradecemos a atenção.\n\nAtt. \n', 0, '2016-06-30 23:08:40', '2016-06-30 23:08:40'),
(89, 'Thiego', 'thiego.barbosa@leadforce.com.br', '11983767869', 'Boa tarde,\n\nGostaria de saber valores para locação de salas privativas a partir de 5 pessoas.', 0, '2016-07-05 21:07:40', '2016-07-05 21:07:40'),
(90, 'Bruno Bindi ', 'bindimed@gmail.com', '11963802423', 'Gostaria de saber os valores de locação de salas, reserva de salas para reuniões eventuais. Grato Bruno . Obs: já conheço o local apenas algumas salas como paris.   ', 0, '2016-07-19 22:53:53', '2016-07-19 22:53:53'),
(91, 'gilberto', 'calunga.sp@gmail.com', '11999367608', 'vendo um terreno na cidade de São Paulo,no bairro de santana ,para construção de LOJA DE MARCA FORTE possivel shopping juntando todos os outros terrenos laterais  R$10.500.000,00\nCONTATO GILBERTO 11 99936-7608', 0, '2016-07-23 15:09:54', '2016-07-23 15:09:54'),
(92, 'Fernando Olinto', 'fo@flexioffice.com.br', '21 21037600', 'Parabéns pelo novo centro. Estamos abertos para uma parceria no Rio de Janeiro. Flexioffice.', 0, '2016-07-26 18:38:30', '2016-07-26 18:38:30'),
(93, 'kelly', 'kelly.galante@helloo.com.br', '98548-9488', 'Boa tarde,\n\nTudo bem? meu nome é Kelly, trabalho na Helloo, mídia em elevadores residenciais na região de Alphaville.\n\nEstamos presentes nos principais prédios residenciais da região e temos uma audiência de 29.400 pessoas/dia.\n\nTemos um ótimo custo benefício, a Helloo, tem o objetivo de atender tanto os grandes, como pequenos anunciantes.\n\nO anunciante pode escolher os condomínios que achar mais interessante.\n\nGostaria muito de explicar um pouco sobre nossa mídia, podemos marcar uma reunião? \n\nAguardo retorno.\n\nMuito obrigada! Abs,', 0, '2016-07-26 23:21:03', '2016-07-26 23:21:03'),
(94, 'Karen Gimenez', 'karen@ccsconsultoria.net', '1145514379', 'Bom dia\n\nCoordeno um grupo de profissionais que se reúne semanalmente - 5as-feiras das 7 até por volta de 9h30 da manhã - são aproximadamente 20 pessoas. Preciso encontrar um novo local para essa reunião e gostaria de saber a disponibilidade das instalações de vocês. Me interesei pelo auditório porque vi pelas imagens que é possíve disponibilizar as cadeiras em U.Haveria mesas para dar suporte a essas cadeiras?. Preciso de um monitor para passar power point e  preferencialmente wi-fi também.  Seia um contrato anual ou semestral com encontroas semanais\n\nGrata\n\nKaren Gimenez', 0, '2016-07-31 08:11:11', '2016-07-31 08:11:11'),
(95, 'Fernando Azevedo', 'fernando.azevedo@styliff.com', '11991125129', 'Olá,\nGostaria de mais informações sobre o Premium Business Plan.\nUm espaço para 3 a 6 pessoas.', 0, '2016-08-04 05:43:06', '2016-08-04 05:43:06'),
(96, 'Lourisval Feliciano', 'pointerassessoria@gmail.com', '11 953210996', 'interessado, ponto de venda escola de mergulho, formação de profissionais de mergulho.\n', 1, '2016-08-11 20:42:30', '2016-08-12 00:41:31');

-- --------------------------------------------------------

--
-- Estrutura da tabela `facilities`
--

CREATE TABLE `facilities` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto_1` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_1_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_2` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_2_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `facilities`
--

INSERT INTO `facilities` (`id`, `texto_1`, `texto_1_en`, `texto_2`, `texto_2_en`, `created_at`, `updated_at`) VALUES
(1, '<p>A Infinity Business Network escolheu um edif&iacute;cio com o conceito inovador de <em>Facilities Office</em>,&nbsp; solu&ccedil;&otilde;es para tornar mais f&aacute;cil e prazeroso o dia-a-dia corporativo,&nbsp; tanto durante o expediente como nas horas livres.</p>\r\n\r\n<p>Sua localiza&ccedil;&atilde;o &eacute; privilegiada, estando na principal via de Alphaville, Alameda Rio Negro,&nbsp;em uma regi&atilde;o com o ISS reduzido e&nbsp;ao lado de importantes rodovias como a Castelo Branco e o Rodoanel. Nosso Business Center est&aacute;&nbsp;cercado&nbsp;de shoppings centers, ao lado do Iguatemi, diversos&nbsp;restaurantes, hot&eacute;is e com&eacute;rcio sofisticado.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '<p><em>More information</em></p>\r\n\r\n<p><em>Infinity Business Network choose a building with the innovative concept of Facilities Office, solutions to make the corporate day to day easier and enjoyable, both during working hours as in the free time.</em></p>\r\n\r\n<p><em>A prestigious address, at the main route of Alphaville, a region with reduced ISS and next to the major highways as Castello Branco and Rodoanel, our Business Centre is surrounded by malls, restaurants, hotels and banks.</em></p>\r\n', '<p>Servi&ccedil;os b&aacute;sicos e em sistema de&nbsp;<em>pay-per-use&nbsp;</em>de concierge, limpeza, courier,&nbsp;administra&ccedil;&atilde;o de eventos, academia, vesti&aacute;rios com chuveiros,&nbsp;quadra de t&ecirc;nis, sal&atilde;o de jogos, sal&atilde;o de beleza, caf&eacute; e restaurante e controle de acesso&nbsp;s&atilde;o algumas das facilidades do edif&iacute;cio que escolhemos estar,&nbsp;visando atender com seguran&ccedil;a e tecnologia as mais diversas necessidades,&nbsp;contribuindo diretamente para a melhora da produtividade de seus usu&aacute;rios.</p>\r\n\r\n<p>Qualidade |&nbsp;Conveni&ecirc;ncia |&nbsp;Lucratividade&nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '<p><em>Basic services and pay-per-use system of concierge, cleaning,</em> <em>courier, event management, fitness room, cloakrooms with showers, tennis court,</em> <em>game room, beauty salon, coffee shop, and access control are some of the building facilities, aiming to meet with security and technology the many different needs,</em> <em>directly contributing to the improvement of the productivity of its users.</em></p>\r\n\r\n<p>Quality | Convenience | Profitability</p>\r\n', NULL, '2016-04-01 23:30:21');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fotos`
--

CREATE TABLE `fotos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `legenda` text COLLATE utf8_unicode_ci,
  `galeria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `fotos`
--

INSERT INTO `fotos` (`id`, `ordem`, `imagem`, `legenda`, `galeria`, `created_at`, `updated_at`) VALUES
(18, 4, 'IMG_9175_20160324194953.jpg', NULL, 'experimente', '2016-03-24 22:49:53', '2016-03-24 22:49:53'),
(19, 5, 'IMG_9264_20160324195005.jpg', NULL, 'experimente', '2016-03-24 22:50:06', '2016-03-24 22:50:06'),
(20, 6, 'IMG_9283_20160324195015.jpg', NULL, 'experimente', '2016-03-24 22:50:15', '2016-03-24 22:50:15'),
(21, 7, 'IMG_9305_20160324195027.jpg', NULL, 'experimente', '2016-03-24 22:50:28', '2016-03-24 22:50:28'),
(24, 3, 'IMG_9068_20160324195115.jpg', NULL, 'contato', '2016-03-24 22:51:16', '2016-03-24 22:51:16'),
(25, 2, 'IMG_9229_20160324195130.jpg', NULL, 'contato', '2016-03-24 22:51:30', '2016-03-24 22:51:30'),
(26, 4, 'IMG_9132_20160324195136.jpg', NULL, 'contato', '2016-03-24 22:51:36', '2016-03-24 22:51:36'),
(30, 3, 'IMG_9086_20160324200537.jpg', NULL, 'conceito', '2016-03-24 23:05:38', '2016-03-24 23:05:38'),
(31, 0, 'IMG_0832_20160504142301.jpg', NULL, 'conceito', '2016-05-04 17:23:02', '2016-05-04 17:23:02'),
(32, 2, 'IMG_9058_20160504142410.jpg', NULL, 'conceito', '2016-05-04 17:24:10', '2016-05-04 17:24:10'),
(35, 1, 'IMG_9030_20160519204916.jpg', NULL, 'conceito', '2016-05-19 23:49:16', '2016-05-19 23:49:16'),
(36, 4, 'IMG_9125_20160519204938.jpg', NULL, 'conceito', '2016-05-19 23:49:38', '2016-05-19 23:49:38'),
(37, 5, 'IMG_9187_20160519205003.jpg', NULL, 'conceito', '2016-05-19 23:50:04', '2016-05-19 23:50:04'),
(39, 7, 'IMG_9324_20160519205059.jpg', NULL, 'conceito', '2016-05-19 23:51:00', '2016-05-19 23:51:00'),
(52, 1, 'IMG_2014_20160613150906.jpg', NULL, 'experimente', '2016-06-13 18:09:07', '2016-06-13 18:09:07'),
(53, 3, 'IMG_2054_20160613150938.jpg', NULL, 'experimente', '2016-06-13 18:09:39', '2016-06-13 18:09:39'),
(54, 2, 'IMG_2040_20160613151003.jpg', NULL, 'experimente', '2016-06-13 18:10:03', '2016-06-13 18:10:03'),
(56, 0, 'IMG_2034_20160613151113.jpg', NULL, 'contato', '2016-06-13 18:11:14', '2016-06-13 18:11:14'),
(58, 1, 'IMG_2068_20160613151155.jpg', NULL, 'contato', '2016-06-13 18:11:56', '2016-06-13 18:11:56'),
(59, 6, 'img-2273_20160817142848.jpg', NULL, 'conceito', '2016-08-17 17:28:49', '2016-08-17 17:28:49'),
(61, 0, 'img-2316_20160817143101.jpg', NULL, 'experimente', '2016-08-17 17:31:02', '2016-08-17 17:31:02'),
(62, 8, 'img-2306_20160817143142.jpg', NULL, 'experimente', '2016-08-17 17:31:43', '2016-08-17 17:31:43');

-- --------------------------------------------------------

--
-- Estrutura da tabela `historico`
--

CREATE TABLE `historico` (
  `id` int(10) UNSIGNED NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `historico`
--

INSERT INTO `historico` (`id`, `texto`, `texto_en`, `created_at`, `updated_at`) VALUES
(1, '<p>Respons&aacute;vel por trazer ao Brasil o conceito de escrit&oacute;rios inteligentes em 1992, o s&oacute;cio-diretor, Mauro Koraicho, criou uma s&oacute;lida empresa com 17 filiais e mais de 15 mil clientes&nbsp;que administrou durante 18 anos.</p>\r\n\r\n<p>Agora, aproveitando o avan&ccedil;o tecnol&oacute;gico dos &uacute;ltimos anos, a Infinity est&aacute; de volta com um novo centro de neg&oacute;cios em cerca de 1500 m2 no rec&eacute;m-inaugurado Empresarial Escrit&oacute;rios Rio Negro, em Alphaville.</p>\r\n\r\n<p>Os clientes Infinity contam com um conceito aprimorado de escrit&oacute;rios prontos, identidade empresarial e eventos de neg&oacute;cios, onde todas as facilidades estruturais e de servi&ccedil;os necess&aacute;rios no dia a dia corporativo est&atilde;o presentes num ambiente de alto padr&atilde;o, preocupado tamb&eacute;m com o bem-estar e o desenvolvimento do <em>networking</em> de cada usu&aacute;rio.</p>\r\n\r\n<p>A Networking Community desenvolvida pela Infinity objetiva integrar as pessoas certas, que podem se ajudar profissionalmente, dar refer&ecirc;ncias e fazer boas indica&ccedil;&otilde;es, atrav&eacute;s da realiza&ccedil;&atilde;o de eventos e <em>happy-hours </em>internos.</p>\r\n\r\n<p>Nosso moderno e flex&iacute;vel espa&ccedil;o contribui diretamente para a melhoria da performance profissional de seus usu&aacute;rios, j&aacute; que n&atilde;o h&aacute; preocupa&ccedil;&otilde;es com o funcionamento operacional, apenas foco integral no trabalho e no relacionamento.</p>\r\n\r\n<p>Tudo isso ao lado do Shopping Iguatemi Alphaville na Alameda Rio Negro, a principal via regi&atilde;o, localizada a apenas 15&nbsp;minutos da cidade de S&atilde;o Paulo e no centro dos tr&ecirc;s maiores aeroportos da regi&atilde;o: Congonhas, Guarulhos e Viracopos.</p>\r\n', '<p><strong><em>Responsible for bringing to Brazil the concept of intelligent offices in 1992, the partner-director, </em></strong><em>Mauro Koraicho, created a solid company with 17 branch offices and more than 15,000 clients in 18 years.</em></p>\r\n\r\n<p><em>Now, taking advantage of the latest technological advances, the Infinity is back with a new business center with 1500m2 in Alphaville.</em></p>\r\n\r\n<p><em>Infinity clients have an enhanced concept of equipped offices, corporate identity and business events where all the structural facilities and companies needs are present in a high standard setting, also concerned about the well-being and the networking development of each user.</em></p>\r\n\r\n<p><em>The Networking Community developed by Infinity</em> <em>aims to integrate the right people that can professionally improve, provide references and make good indications, by promoting events and happy hours.</em></p>\r\n\r\n<p><em>Our modern and flexible place directly contributes to the professional improvement of its members, since there is no concern about the operational issues, only full focus on work and relationships.</em></p>\r\n\r\n<p><em>All of this beside the Shopping Iguatemi Alphaville at Alameda Rio Negro, the main route area, located just 15 minutes from the city of Sao Paulo and in the center of the largest three airports in the region: Congonhas, Guarulhos and Viracopos.</em></p>\r\n', NULL, '2016-04-01 17:35:10');

-- --------------------------------------------------------

--
-- Estrutura da tabela `indicacoes`
--

CREATE TABLE `indicacoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `indicado` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `por` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `indicacoes`
--

INSERT INTO `indicacoes` (`id`, `indicado`, `por`, `created_at`, `updated_at`) VALUES
(1, 'teste@teste.com.br', 'teste@teste.com.br', '2016-03-29 00:23:48', '2016-03-29 00:23:48'),
(2, 'teste@teste.com.br', 'teste@teste.com.br', '2016-04-06 03:17:27', '2016-04-06 03:17:27'),
(3, 'teste@teste.com.br', 'teste@teste.com.br', '2016-04-06 03:36:30', '2016-04-06 03:36:30'),
(4, 'teste@teste.com.br', 'teste@teste.com.br', '2016-04-06 15:26:57', '2016-04-06 15:26:57'),
(5, 'teste@teste.com.br', 'teste@teste.com.br', '2016-04-06 15:27:53', '2016-04-06 15:27:53'),
(6, 'teste@teste.com.br', 'teste@teste.com.br', '2016-04-06 15:32:37', '2016-04-06 15:32:37'),
(7, 'teste@teste.com.br', 'teste@teste.com.br', '2016-04-06 15:33:55', '2016-04-06 15:33:55'),
(8, 'teste@teste.com.br', 'teste@teste.com.br', '2016-04-06 16:04:16', '2016-04-06 16:04:16'),
(9, 'teste@teste.com.br', 'teste@teste.com.br', '2016-04-06 17:58:35', '2016-04-06 17:58:35'),
(10, 'teste@teste.com.br', 'teste@teste.com.br', '2016-04-08 06:33:59', '2016-04-08 06:33:59'),
(11, 'teste@teste.com.br', 'teste@teste.com.br', '2016-04-08 07:18:39', '2016-04-08 07:18:39'),
(12, 'teste@teste.com.br', 'teste@teste.com.br', '2016-04-08 23:51:27', '2016-04-08 23:51:27'),
(13, 'teste@teste.com.br', 'teste@teste.com.br', '2016-04-11 18:21:30', '2016-04-11 18:21:30'),
(14, 'teste@teste.com.br', 'teste@teste.com.br', '2016-04-12 15:59:49', '2016-04-12 15:59:49'),
(15, 'teste@teste.com.br', 'teste@teste.com.br', '2016-04-12 17:09:24', '2016-04-12 17:09:24'),
(16, 'allersongoncalves@hotmail.com', 'tonavip00@hotmail.com', '2016-04-15 13:58:11', '2016-04-15 13:58:11');

-- --------------------------------------------------------

--
-- Estrutura da tabela `midia`
--

CREATE TABLE `midia` (
  `id` int(10) UNSIGNED NOT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `veiculo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `midia`
--

INSERT INTO `midia` (`id`, `data`, `veiculo`, `titulo`, `imagem`, `link`, `created_at`, `updated_at`) VALUES
(2, '2016-08', 'teste', 'teste', '', '', '2016-08-18 21:30:59', '2016-08-18 21:30:59');

-- --------------------------------------------------------

--
-- Estrutura da tabela `midia_imagens`
--

CREATE TABLE `midia_imagens` (
  `id` int(10) UNSIGNED NOT NULL,
  `midia_id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2016_03_01_170622_create_servicos_table', 1),
('2016_03_02_034737_create_indicacoes_table', 1),
('2016_03_02_040119_create_solicitacoes_de_test_drive_table', 1),
('2016_03_02_043142_create_planos_table', 1),
('2016_03_02_185620_create_fotos_table', 1),
('2016_03_02_195530_create_perfis_table', 1),
('2016_03_02_195913_create_perfis_planos_table', 1),
('2016_03_02_204736_create_comparativo_table', 1),
('2016_03_02_204756_create_comparativo_planos_table', 1),
('2016_03_04_222323_create_facilities_table', 1),
('2016_03_07_165736_create_historico_table', 1),
('2016_03_10_020656_create_video_table', 1),
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2016_03_01_170622_create_servicos_table', 1),
('2016_03_02_034737_create_indicacoes_table', 1),
('2016_03_02_040119_create_solicitacoes_de_test_drive_table', 1),
('2016_03_02_043142_create_planos_table', 1),
('2016_03_02_185620_create_fotos_table', 1),
('2016_03_02_195530_create_perfis_table', 1),
('2016_03_02_195913_create_perfis_planos_table', 1),
('2016_03_02_204736_create_comparativo_table', 1),
('2016_03_02_204756_create_comparativo_planos_table', 1),
('2016_03_04_222323_create_facilities_table', 1),
('2016_03_07_165736_create_historico_table', 1),
('2016_03_10_020656_create_video_table', 1),
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2016_03_01_170622_create_servicos_table', 1),
('2016_03_02_034737_create_indicacoes_table', 1),
('2016_03_02_040119_create_solicitacoes_de_test_drive_table', 1),
('2016_03_02_043142_create_planos_table', 1),
('2016_03_02_185620_create_fotos_table', 1),
('2016_03_02_195530_create_perfis_table', 1),
('2016_03_02_195913_create_perfis_planos_table', 1),
('2016_03_02_204736_create_comparativo_table', 1),
('2016_03_02_204756_create_comparativo_planos_table', 1),
('2016_03_04_222323_create_facilities_table', 1),
('2016_03_07_165736_create_historico_table', 1),
('2016_03_10_020656_create_video_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `perfis`
--

CREATE TABLE `perfis` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nome_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `perfis`
--

INSERT INTO `perfis` (`id`, `ordem`, `nome`, `nome_en`, `slug`, `texto`, `texto_en`, `created_at`, `updated_at`) VALUES
(1, 0, 'Startup', 'STARTUP ', 'startup', '<p>Comece seu neg&oacute;cio dentro de um ambiente estruturado, que passe seguran&ccedil;a para seus clientes e fornecedores&nbsp;e que propicie&nbsp;relacionamentos profissionais.&nbsp;</p>\r\n', '<p>Start your business in a structured environment, transmitting security to your clients and suppliers and provide professional relationships.</p>\r\n', '2016-03-16 15:10:37', '2016-04-01 17:28:38'),
(2, 1, 'Pequena ou Média Empresa', 'SMALL OR MEDIUM COMPANY ', 'pequena-ou-media-empresa', '<p>Estrutura completa de espa&ccedil;o, servi&ccedil;os e tecnologia capaz de&nbsp;otimizar a opera&ccedil;&atilde;o da sua empresa, contribuindo&nbsp;com a produtividade dos funcion&aacute;rios e potencializando&nbsp;as chances de crescimento no mercado.</p>\r\n', '<p>Structured environment, services and technology able to optimize the operation of your company, contributing to the productivity of employees and enhancing the growth chances in the market.</p>\r\n', '2016-03-16 15:10:44', '2016-04-01 17:29:05'),
(3, 2, 'Empresa de Grande Porte', 'LARGE-SIZED COMPANY ', 'empresa-de-grande-porte', '<p>Servi&ccedil;os de qualidade em espa&ccedil;o corporativo completo para opera&ccedil;&atilde;o&nbsp;em tempo integral, projetos especiais, reestrutura&ccedil;&otilde;es (<em>downsizing</em> e expans&otilde;es), reuni&otilde;es, treinamentos e eventos.&nbsp;</p>\r\n', '<p>Quality services in full corporate area for full-time operation, special projects, reorganizations<em> (<em>downsizing and expansions), meetings, training and events.</em></em></p>\r\n', '2016-03-16 15:10:51', '2016-04-01 17:29:00'),
(4, 3, 'ONG', 'ONG', 'ong', '<p>Ganhe for&ccedil;a para atrair investimentos em local profissional estruturado para reuni&otilde;es internas, recep&ccedil;&atilde;o de convidados e atividades do cotidiano em ambiente 100%&nbsp;corporativo e bem localizado.</p>\r\n', '<p>Get strength to attract investments in a structured professional place for internal meetings, welcoming guests and daily activities in a 100% corporate environment.</p>\r\n', '2016-03-16 15:10:56', '2016-04-01 17:29:17'),
(5, 4, 'Profissional Autônomo', 'PROFESSIONAL INDEPENDENT ', 'profissional-autonomo', '<p>Ambiente profissional&nbsp;completo, com infraestrutura&nbsp;e servi&ccedil;os de alta qualidade sob demanda, para facilitar as atividades do dia-a-dia e otimizar a sua imagem corporativa.</p>\r\n', '<p>Complete professional environment with infrastructure and high quality services on-demand in order to get easier the daily activities and optimize its corporate image.</p>\r\n', '2016-03-16 15:11:03', '2016-04-01 17:29:28'),
(6, 5, 'Home-Office', 'HOME OFFICE ', 'home-office', '<p>Troque a informalidade caseira por&nbsp;um espa&ccedil;o dedicado a profissionais que visam maior produtividade e destaque&nbsp;no mercado, contando diariamente com uma&nbsp;estrutura corporativa&nbsp;completa para trabalhar e servi&ccedil;os de qualidade&nbsp;sob demanda para realizar reuni&otilde;es e eventos.</p>\r\n', '<p>Change the homemade informality for an area dedicated to professionals who seek greater productivity and prominence in the market counting daily with a complete corporate structure to work and quality services on demand to hold meetings and events.</p>\r\n', '2016-03-16 15:11:08', '2016-04-01 17:29:39');

-- --------------------------------------------------------

--
-- Estrutura da tabela `perfis_planos`
--

CREATE TABLE `perfis_planos` (
  `id` int(10) UNSIGNED NOT NULL,
  `perfil_id` int(10) UNSIGNED NOT NULL,
  `plano_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `perfis_planos`
--

INSERT INTO `perfis_planos` (`id`, `perfil_id`, `plano_id`, `created_at`, `updated_at`) VALUES
(2, 1, 2, '2016-03-16 15:10:37', '2016-03-16 15:10:37'),
(3, 1, 3, '2016-03-16 15:10:37', '2016-03-16 15:10:37'),
(4, 1, 4, '2016-03-16 15:10:37', '2016-03-16 15:10:37'),
(5, 1, 5, '2016-03-16 15:10:37', '2016-03-16 15:10:37'),
(7, 1, 7, '2016-03-16 15:10:37', '2016-03-16 15:10:37'),
(8, 2, 1, '2016-03-16 15:10:44', '2016-03-16 15:10:44'),
(12, 2, 5, '2016-03-16 15:10:44', '2016-03-16 15:10:44'),
(14, 2, 7, '2016-03-16 15:10:44', '2016-03-16 15:10:44'),
(15, 3, 1, '2016-03-16 15:10:51', '2016-03-16 15:10:51'),
(19, 3, 5, '2016-03-16 15:10:51', '2016-03-16 15:10:51'),
(21, 3, 7, '2016-03-16 15:10:51', '2016-03-16 15:10:51'),
(23, 4, 2, '2016-03-16 15:10:56', '2016-03-16 15:10:56'),
(24, 4, 3, '2016-03-16 15:10:56', '2016-03-16 15:10:56'),
(26, 4, 5, '2016-03-16 15:10:56', '2016-03-16 15:10:56'),
(28, 4, 7, '2016-03-16 15:10:56', '2016-03-16 15:10:56'),
(29, 5, 1, '2016-03-16 15:11:03', '2016-03-16 15:11:03'),
(30, 5, 2, '2016-03-16 15:11:03', '2016-03-16 15:11:03'),
(31, 5, 3, '2016-03-16 15:11:03', '2016-03-16 15:11:03'),
(33, 5, 5, '2016-03-16 15:11:03', '2016-03-16 15:11:03'),
(35, 5, 7, '2016-03-16 15:11:03', '2016-03-16 15:11:03'),
(37, 6, 2, '2016-03-16 15:11:08', '2016-03-16 15:11:08'),
(38, 6, 3, '2016-03-16 15:11:08', '2016-03-16 15:11:08'),
(40, 6, 5, '2016-03-16 15:11:08', '2016-03-16 15:11:08'),
(42, 6, 7, '2016-03-16 15:11:08', '2016-03-16 15:11:08'),
(43, 1, 6, '2016-03-21 19:38:35', '2016-03-21 19:38:35'),
(44, 2, 6, '2016-03-21 19:40:21', '2016-03-21 19:40:21'),
(45, 3, 6, '2016-03-21 19:42:27', '2016-03-21 19:42:27'),
(46, 4, 4, '2016-03-21 19:48:17', '2016-03-21 19:48:17'),
(47, 4, 6, '2016-03-21 19:48:17', '2016-03-21 19:48:17'),
(48, 5, 4, '2016-03-21 19:50:55', '2016-03-21 19:50:55'),
(49, 5, 6, '2016-03-21 19:50:55', '2016-03-21 19:50:55'),
(50, 6, 4, '2016-03-21 19:51:53', '2016-03-21 19:51:53'),
(51, 6, 6, '2016-03-21 19:51:53', '2016-03-21 19:51:53');

-- --------------------------------------------------------

--
-- Estrutura da tabela `planos`
--

CREATE TABLE `planos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nome_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sigla` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tipo` int(11) NOT NULL,
  `valor` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada_en` text COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `texto_en` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `planos`
--

INSERT INTO `planos` (`id`, `nome`, `nome_en`, `slug`, `sigla`, `tipo`, `valor`, `chamada`, `chamada_en`, `texto`, `texto_en`, `created_at`, `updated_at`) VALUES
(1, 'Premium Business Plan', 'Premium Business Plan', 'premium-business-plan', 'PBP', 1, '1.500,00', 'Escritórios prontos exclusivos, equipados com móveis de alto padrão, tecnologia de ponta e serviços personalizados. Contratos de locação flexíveis e sem investimento inicial.', 'Fully equipped and tech powered offices with high standard furniture and personalized services. Flexible leasing contracts, without initial investment.', '<p>Salas privativas de 11 a 50 m2 com acesso 24/7 e&nbsp;esta&ccedil;&otilde;es de trabalho completas (mesa, cadeira, gaveteiro, internet, linha e aparelho telef&ocirc;nico digital com atendimento personalizado, redirecionamento de liga&ccedil;&otilde;es&nbsp;e caixa postal com acesso remoto).&nbsp;&Aacute;rea comum (Infinity + Condom&iacute;nio) com mais de 1500 m2 (recep&ccedil;&atilde;o/ caf&eacute;/ salas de reuni&atilde;o/ banheiros/ copas/ dep&oacute;sitos/ academia/ vesti&aacute;rio/ quadra de t&ecirc;nis/ sala de jogos/ restaurante/ sal&atilde;o de beleza).&nbsp;Servi&ccedil;os de copa, limpeza, e manuten&ccedil;&atilde;o interna de TI (rede e telefonia) e gerenciamento de escrit&oacute;rio.&nbsp;Servi&ccedil;os sob demanda de secretariado/ courier/ concierge/ organiza&ccedil;&atilde;o de eventos, estacionamento com manobrista, entre outros.&nbsp;Acesso aos eventos de networking oferecidos pela Infinity (palestras/ cursos/ happy hours, etc.) e a rede de relacionamento corporativo da Infinity. 04&nbsp;horas gratuitas por m&ecirc;s de sala de reuni&atilde;o com TV para at&eacute; 07 pessoas sujeito &agrave; disponibilidade.&nbsp;Pre&ccedil;os especiais para loca&ccedil;&atilde;o de espa&ccedil;os de reuni&otilde;es, treinamentos, showroom e eventos empresariais.</p>\r\n', '<p><em>Private rooms of 11-50 m2 with 24/7 access and ready to use workstations (desk, chair, drawers, internet, line and digital telephone set with personalized assistance, redirecting of calls and voicemail with remote access). Common area (Infinity + Condominium) with more than 1500 m2 (reception/ coffee/ meeting rooms/</em> <em>bathrooms/ catering service / fitness room/ cloakrooms/ tennis court/ game room/ coffee shop/ beauty salon). Catering, cleaning and internal IT (network and telecom) and office management. On demand secretarial services/ courier/ concierge/ event organization, valet parking, among others.</em> <em>Access to the networking events provided by Infinity (presentations/ courses/ happy hours, and so on) and the corporate relationship network of Infinity. 04 free monthly hours of meeting room with TV for up to 07 people, upon availability. Special prices for meeting rooms, training, showroom and corporate events.</em></p>\r\n', NULL, '2016-04-01 23:38:37'),
(2, 'Shared Business Plan', 'Shared Business Plan', 'shared-business-plan', 'SBP', 1, '1.400,00', 'Estações de trabalho privativas completas em ambiente executivo compartilhado com acesso 24 horas / 7 dias na semana.', 'Private workstations in a shared executive room with 24/ access.', '<p>Sala fechada (cart&atilde;o de acesso individual) com&nbsp;esta&ccedil;&otilde;es de trabalho completas (mesa, cadeira, gaveteiro&nbsp;e aparelho telef&ocirc;nico digital com senha e caixa postal individuais) e arm&aacute;rios sob demanda.&nbsp;Linha telef&ocirc;nica exclusiva com atendimento personalizado, caixa postal com acesso remoto e&nbsp;transfer&ecirc;ncia de liga&ccedil;&otilde;es.&nbsp;Uso compartilhado da &aacute;rea comum (Infinity + Condom&iacute;nio) com cerca de 1500m2 (recep&ccedil;&atilde;o/ caf&eacute;/ salas de reuni&atilde;o/ banheiros/ copas/ dep&oacute;sitos/ academia/ vesti&aacute;rio/ quadra de t&ecirc;nis/ sala de jogos/ restaurante/ sal&atilde;o de beleza); Acesso a servi&ccedil;os copa, limpeza, manuten&ccedil;&atilde;o de TI (rede e telefonia), gerenciamento de escrit&oacute;rio e, sob demanda, de secretariado/ courier/ concierge/ organiza&ccedil;&atilde;o de eventos, estacionamento com manobrista, entre outros.&nbsp;Acesso aos eventos de Networking oferecidos pela Infinity (palestras/ cursos/ happy hours, etc.) e a rede de relacionamento corporativo da Infinity.</p>\r\n', '<p><em>Private room (individual access card)</em> <em>with ready to use workstations (desk, chair, drawers and digital telephone equipment with password and individual voicemail) and cabinets on demand. Dedicated telephone line with personalized service, voicemail with remote access and calls transferring.</em> <em>Shared use of common area (Infinity + Condominium) with approximately 1500 m2 (reception/ coffee/ meeting rooms/</em> <em>bathrooms/ catering service / fitness room/ cloakrooms/ tennis court/ game room/ coffee shop/ beauty salon); Access to catering, cleaning, IT (network and telecom), office management. Access to the networking events provided by Infinity (presentations/ courses/ happy hours, and so on) and the corporate relationship network of Infinity.</em></p>\r\n', NULL, '2016-04-01 17:32:27'),
(3, 'Coworking Business Plan', 'Coworking Business Plan', 'coworking-business-plan', 'CBP', 1, '1.000,00', 'Estações de trabalho rotativas em ambiente executivo compartilhado com acesso livre em dias úteis, das 9h às 18h.', 'Open space in the common area of Infinity, with rotating workstations with free access in working days from 9 am to 6 pm. ', '<p>Ambiente aberto com&nbsp;posi&ccedil;&otilde;es de trabalho localizadas na &aacute;rea comum da Infinity.&nbsp;Hot desking com mesas, poltronas e acesso &agrave; rede wi-fi&nbsp;para uso sempre que necess&aacute;rio. Possibilidade de contrata&ccedil;&atilde;o de servi&ccedil;os extras (salas de reuni&otilde;es, treinamento, showroom e eventos/ secretariado/ courier/ concierge/ organiza&ccedil;&atilde;o de eventos/ estacionamento com manobrista/ etc.) sempre que necess&aacute;rio, mediante&nbsp;disponibilidade.</p>\r\n', '<p><em>Hot desking with tables, armchairs and wi-fi access for use whenever you need. Possibility of hiring extra services (meeting rooms, training, showroom and events/ secretarial services/ courier/ concierge/ event organization, valet parking/ and so on),</em> <em>whenever you need, upon availability.</em></p>\r\n', NULL, '2016-04-01 17:32:42'),
(4, 'Virtual Business Plan', 'Virtual Business Plan', 'virtual-business-plan', 'VBP', 1, '400,00', 'Endereço empresarial privilegiado com linha telefônica exclusiva e atendimento bilíngue personalizado. Escritórios e salas de reuniões à disposição sob demanda. Acesso livre das 9h às 18h em dias úteis.', 'Prestigious business address with dedicated telephone line and personalized bilingual answering. Offices and meeting rooms on demand. Free access from 9 am to 6 pm in working days. ', '<p>Endere&ccedil;o corporativo de prest&iacute;gio, com ISS reduzido, para fins comerciais e fiscais.&nbsp;Linha telef&ocirc;nica privada com atendimento bil&iacute;ngue personalizado, caixa postal com acesso remoto e redirecionamento de liga&ccedil;&otilde;es. Caixa de correspond&ecirc;ncia exclusiva.&nbsp;Acesso em hor&aacute;rio comercial a &aacute;rea comum da Infinity com cerca de 500 m2 (recep&ccedil;&atilde;o/ caf&eacute;/ salas de reuni&atilde;o/ audit&oacute;rio/ banheiros/ copas/ dep&oacute;sitos/salas de reuni&atilde;o, treinamento e showroom).&nbsp;Acesso a servi&ccedil;os sob demanda de secretariado/ courier/ concierge/ organiza&ccedil;&atilde;o de eventos, estacionamento com manobrista, entre outros.&nbsp;</p>\r\n', '<p><em>Prestigious corporate address with reduced ISS location, for commercial and tax purposes.</em> <em>Dedicated telephone line with personalized bilingual answering, voicemail with remote access and redirecting of calls.</em> <em>Exclusive mailbox. Business hours access to Infinity common area with approximately 500 m2 (reception/ coffee/</em> <em>meeting rooms/ auditorium/ bathrooms/ catering service / meeting rooms, training, showroom). On demand secretarial services/ courier/ concierge/ event organization, valet parking, among others.</em></p>\r\n', NULL, '2016-04-01 23:31:54'),
(5, 'Digital Signage Plan', 'Digital Signage Plan', 'digital-signage-plan', '', 2, '', 'Produção e divulgação de vídeos, propagandas, apresentações e animações corporativas reproduzidas nos monitores e videowall do centro de negócio. Inclusão da marca no diretório Infinity.  INVESTIMENTOS SOB CONSULTA.', 'Production and distribution of corporate videos, advertisements, presentations and animations reproduced on the monitors and videowall of the business center. Be part of INFINITY directory. ​​​​​​​', '<p>Apresenta&ccedil;&otilde;es, v&iacute;deos e anima&ccedil;&otilde;es corporativas personalizadas reproduzidas para rede de network da Infinity Business atrav&eacute;s do diret&oacute;rio interno, nos&nbsp;monitores e&nbsp;videowall&nbsp;do centro de neg&oacute;cio. Parceria com empresa de produ&ccedil;&atilde;o de material digital.</p>\r\n', '<p><em>Customized corporate presentations, videos and animations </em><em>on the monitors and videowall of the business center.</em> <em>Partnership with digital company.&nbsp;INVESTMENTS UNDER CONSULTATION.</em></p>\r\n', NULL, '2016-04-01 23:35:06'),
(6, 'Networking', 'Networking', 'networking', '', 2, '', 'Acesso à rede de relacionamento Infinity.', 'Join to the Infinity community.', '<p>Ingresso na comunidade&nbsp;Infinity, atrav&eacute;s de eventos peri&oacute;dicos com conte&uacute;do de qualidade e happy-hours que objetivam ampliar a rede de relacionamento profissional dos participantes.</p>\r\n', '<p><em>Join to the Infinity</em> <em>community through periodic events with quality content and happy-hours</em> <em>which aims to extend the professional relationship network of the participants.</em></p>\r\n', NULL, '2016-04-01 17:34:03'),
(7, 'Eventos e Reuniões', 'Events and Meetings', 'eventos-e-reunioes', '', 2, '', 'Modernas e equipadas salas de reuniões, auditórios e espaços para conferências, showrooms, workshops e treinamentos com serviços de coffee-break e videoconferência.', 'Modern and equipped meeting rooms, auditoriums and rooms for conferences, showrooms, workshops and trainings with coffee-break and videoconference services.', '<p><strong>Or&ccedil;amentos personalizados.&nbsp;A PARTIR DE R$ 50,00/HORA.</strong></p>\r\n', '<p><em>Customized budgets.</em> <em>AS OF R$ 50,00/HOUR.</em></p>\r\n', NULL, '2016-04-01 17:34:28');

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos`
--

CREATE TABLE `servicos` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `servicos`
--

INSERT INTO `servicos` (`id`, `ordem`, `titulo`, `titulo_en`, `created_at`, `updated_at`) VALUES
(1, 0, 'Acesso 24/7', '24/7 access ', '2016-03-16 15:17:53', '2016-04-01 17:41:11'),
(2, 1, 'Contratos Flexíveis', 'Flexible contracts ', '2016-03-16 15:18:01', '2016-04-01 17:41:18'),
(3, 9, 'Solicitações Sob Demanda', 'On Demand Requests ', '2016-03-16 15:18:11', '2016-04-01 17:42:06'),
(4, 11, 'Equipe Bilíngue', 'Bilingual Staff ', '2016-03-16 15:18:17', '2016-04-01 17:42:20'),
(5, 17, 'Transferência de Ligações', 'Calls Transferring ', '2016-03-16 15:18:24', '2016-04-01 17:43:09'),
(6, 12, 'Atendimento Personalizado', 'Customized Service ', '2016-03-16 15:18:32', '2016-04-01 17:42:29'),
(7, 13, 'Secretariado/Courier/Concierge', 'Secretarial Service/Courier/Concierge ', '2016-03-17 16:12:50', '2016-04-01 17:42:39'),
(8, 5, 'Fatura Única Mensal', 'Single Monthly Invoice ', '2016-03-17 16:12:56', '2016-04-01 17:41:43'),
(9, 6, 'Administração de Espaço e Pessoas', 'Administration of Space and People ', '2016-03-17 16:13:09', '2016-04-01 17:41:49'),
(10, 14, 'Caixa-Postal com Acesso Remoto', 'Voicemail with Remote Access ', '2016-03-17 16:13:18', '2016-04-01 17:42:45'),
(11, 16, 'Administração de Correspondências', 'Mailings Management', '2016-03-17 16:13:30', '2016-04-01 17:43:00'),
(12, 18, 'Serviço de Copa e Limpeza', 'Catering and Cleaning Service ', '2016-03-17 16:13:40', '2016-04-01 17:43:14'),
(13, 21, 'Espaço de Descompressão', 'Decompression Area ', '2016-03-17 16:13:52', '2016-04-01 17:43:38'),
(14, 2, 'Sem Fiador ou Seguro Fiança', 'No Guarantor or Rental Insurance ', '2016-03-17 16:14:00', '2016-04-01 17:41:23'),
(15, 19, 'Área de Copa e Café', 'Coffee Area ', '2016-03-17 16:14:15', '2016-04-01 17:43:20'),
(16, 22, 'Serviços Gráficos ', 'Printing Services ', '2016-03-17 16:14:25', '2016-04-01 17:43:46'),
(17, 24, 'Auditórios com Projeção', 'Auditoriums with Projection ', '2016-03-17 16:14:42', '2016-04-01 17:43:59'),
(18, 23, 'Espaços Equipados para Reuniões ', 'Professional Meeting Rooms ', '2016-03-17 16:14:54', '2016-04-01 17:43:52'),
(19, 26, 'Coffee-break ', 'Coffee-break', '2016-03-17 16:15:11', '2016-04-01 17:44:14'),
(20, 25, 'Videoconferência  ', 'Videoconference ', '2016-03-17 16:15:22', '2016-04-01 17:44:07'),
(21, 27, 'Lockers Individuais', 'Individual Lockers', '2016-03-17 16:15:38', '2016-04-01 17:44:45'),
(23, 15, 'Mail Box Privado', 'Private Mailbox ', '2016-03-22 18:09:02', '2016-04-01 17:42:52'),
(24, 20, 'Vending Machines', 'Vending Machines ', '2016-03-22 18:10:06', '2016-04-01 17:43:28'),
(25, 7, 'Telefonia Digital', 'Digital Telecom ', '2016-03-22 18:12:40', '2016-04-01 17:41:54'),
(26, 8, 'Tecnologia de Ponta', 'Latest Technology', '2016-03-22 18:12:58', '2016-04-01 17:42:00'),
(27, 3, 'Início Imediato', 'Immediate start ', '2016-03-22 18:25:20', '2016-04-01 17:41:30'),
(28, 4, 'Endereço com ISS Reduzido', 'Address with Reduced ISS', '2016-03-22 18:51:48', '2016-04-01 17:41:37'),
(29, 10, 'Estacionamento e Segurança', 'Parking and Security ', '2016-03-22 18:56:57', '2016-04-01 17:42:12');

-- --------------------------------------------------------

--
-- Estrutura da tabela `solicitacoes_de_test_drive`
--

CREATE TABLE `solicitacoes_de_test_drive` (
  `id` int(10) UNSIGNED NOT NULL,
  `data` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estacao_de_trabalho` tinyint(1) NOT NULL DEFAULT '0',
  `sala_de_reuniao` tinyint(1) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `e_mail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `solicitacoes_de_test_drive`
--

INSERT INTO `solicitacoes_de_test_drive` (`id`, `data`, `estacao_de_trabalho`, `sala_de_reuniao`, `nome`, `e_mail`, `telefone`, `lido`, `created_at`, `updated_at`) VALUES
(1, '08/04/2016', 1, 0, 'teste', 'teste@teste.com.br', '111111111111', 0, '2016-04-08 06:30:48', '2016-04-08 06:30:48'),
(2, '08/04/2016', 1, 0, 'teste', 'teste@teste.com.br', '111111111111', 0, '2016-04-08 23:53:43', '2016-04-08 23:53:43'),
(3, '11/4/2016', 1, 0, 'teste', 'teste@teste.com.br', '111111111111', 0, '2016-04-11 18:31:48', '2016-04-11 18:31:48'),
(4, '11/4/2016', 0, 0, 'teste', 'teste@teste.com.br', '111111111111', 0, '2016-04-11 18:36:48', '2016-04-11 18:36:48'),
(5, '12/04/2016', 1, 0, 'teste', 'teste@teste.com.br', '111111111111', 0, '2016-04-12 15:59:00', '2016-04-12 15:59:00'),
(6, '08/04/2016', 1, 0, 'teste', 'teste@teste.com.br', '111111111111', 0, '2016-04-12 17:04:45', '2016-04-12 17:04:45'),
(7, '27/04/2016', 0, 0, 'fabiane satler', 'fabiane.satler@gmail.com', '(11) 973124591', 0, '2016-04-14 23:31:49', '2016-04-14 23:31:49'),
(8, '26042016', 1, 0, 'Anderson Boaventura', 'a.boaventura@icloud.com', '11970875953', 0, '2016-04-26 19:05:33', '2016-04-26 19:05:33'),
(9, '28042016', 1, 1, 'leopoldo da silva martins', 'secretaria@languagesbrasil.com', '11954547640', 0, '2016-04-27 02:52:02', '2016-04-27 02:52:02'),
(10, '07/05/2016', 1, 0, 'Menezes', 'menezes@sudesti.com.br', '11 947888148', 0, '2016-05-01 03:32:28', '2016-05-01 03:32:28'),
(11, '10/05/2016', 1, 0, 'Lene', 'lenefarias2@hotmail.com', '11988781430', 0, '2016-05-06 01:43:51', '2016-05-06 01:43:51'),
(12, '10/05/2016', 1, 0, 'Marcelo Trovati ', 'mtrovati@me.com', '96451-8994', 0, '2016-05-11 08:44:31', '2016-05-11 08:44:31'),
(13, '23/05/2016', 1, 0, 'Luciana Alves', 'lucianafigueiroa@outlook.com', '11 98400 3078', 0, '2016-05-12 03:13:31', '2016-05-12 03:13:31'),
(14, '16/05/2016', 1, 1, 'Carolina', 'carolcanute@hotmail.com', '11989354108', 0, '2016-05-15 00:02:39', '2016-05-15 00:02:39'),
(15, '18/05/2016', 0, 1, 'Patrícia melo', 'viagenspelomundo16@gmail.com', '551148722956', 0, '2016-05-18 01:44:04', '2016-05-18 01:44:04'),
(16, '15/06/2016', 0, 1, 'patricia melo', 'viagenspelomundo16@gmail.com', '11959078570', 0, '2016-06-10 22:50:37', '2016-06-10 22:50:37'),
(17, '12/11/1990', 0, 1, 'Teste', 'teste@veinteractive.com', 'xxxx-xxxx', 0, '2016-08-08 21:25:07', '2016-08-08 21:25:07'),
(18, '12/08/2016', 0, 1, 'Eduardo Rios Garcia', 'eduardorios.garcia@gmail.com', '1141831099', 0, '2016-08-10 02:39:41', '2016-08-10 02:39:41');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$6rIQCrsu0aHQYH9kE5xO4eZl7njKVz/c93QsYlDwQFK8a7r/bNKuu', 'nnJnI65twNns8ryCuWUf0HG21iij5LYnRb1WLkL3jyOZeJ5b4VxeMup6rii6', NULL, '2016-03-17 16:46:39'),
(2, 'infinity', 'contato@infinitybysiness.com.br', '$2y$10$7oyH8NKcgd18SpTcOv/hM.wO7TbJW.Sr68Jbn7nVdlW.P.MHK.4XG', 'vhP90W1Ohy166YnmyaiO1se5P0JvBZ8F4Cdhbv8TfWibCaJI8gF1D6FV3boW', '2016-03-17 16:46:32', '2016-08-17 17:32:55');

-- --------------------------------------------------------

--
-- Estrutura da tabela `video`
--

CREATE TABLE `video` (
  `id` int(10) UNSIGNED NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_codigo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `video_codigo_en` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `video`
--

INSERT INTO `video` (`id`, `imagem`, `video_tipo`, `video_codigo`, `video_codigo_en`, `created_at`, `updated_at`) VALUES
(1, 'bg-infinitybusiness_20160317132954.jpg', 'youtube', 'QFpeED1GMPo', 'IqncBDz-r6k', NULL, '2016-06-14 16:04:38');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comparativo`
--
ALTER TABLE `comparativo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comparativo_planos`
--
ALTER TABLE `comparativo_planos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comparativo_planos_comparativo_id_foreign` (`comparativo_id`),
  ADD KEY `comparativo_planos_plano_id_foreign` (`plano_id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `facilities`
--
ALTER TABLE `facilities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fotos`
--
ALTER TABLE `fotos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `historico`
--
ALTER TABLE `historico`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `indicacoes`
--
ALTER TABLE `indicacoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `midia`
--
ALTER TABLE `midia`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `midia_imagens`
--
ALTER TABLE `midia_imagens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `midia_imagens_midia_id_foreign` (`midia_id`);

--
-- Indexes for table `perfis`
--
ALTER TABLE `perfis`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `perfis_planos`
--
ALTER TABLE `perfis_planos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `perfis_planos_perfil_id_foreign` (`perfil_id`),
  ADD KEY `perfis_planos_plano_id_foreign` (`plano_id`);

--
-- Indexes for table `planos`
--
ALTER TABLE `planos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servicos`
--
ALTER TABLE `servicos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `solicitacoes_de_test_drive`
--
ALTER TABLE `solicitacoes_de_test_drive`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comparativo`
--
ALTER TABLE `comparativo`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `comparativo_planos`
--
ALTER TABLE `comparativo_planos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;
--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT for table `facilities`
--
ALTER TABLE `facilities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `fotos`
--
ALTER TABLE `fotos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;
--
-- AUTO_INCREMENT for table `historico`
--
ALTER TABLE `historico`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `indicacoes`
--
ALTER TABLE `indicacoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `midia`
--
ALTER TABLE `midia`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `midia_imagens`
--
ALTER TABLE `midia_imagens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `perfis`
--
ALTER TABLE `perfis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `perfis_planos`
--
ALTER TABLE `perfis_planos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `planos`
--
ALTER TABLE `planos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `servicos`
--
ALTER TABLE `servicos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `solicitacoes_de_test_drive`
--
ALTER TABLE `solicitacoes_de_test_drive`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `comparativo_planos`
--
ALTER TABLE `comparativo_planos`
  ADD CONSTRAINT `comparativo_planos_comparativo_id_foreign` FOREIGN KEY (`comparativo_id`) REFERENCES `comparativo` (`id`),
  ADD CONSTRAINT `comparativo_planos_plano_id_foreign` FOREIGN KEY (`plano_id`) REFERENCES `planos` (`id`);

--
-- Limitadores para a tabela `midia_imagens`
--
ALTER TABLE `midia_imagens`
  ADD CONSTRAINT `midia_imagens_midia_id_foreign` FOREIGN KEY (`midia_id`) REFERENCES `midia` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `perfis_planos`
--
ALTER TABLE `perfis_planos`
  ADD CONSTRAINT `perfis_planos_perfil_id_foreign` FOREIGN KEY (`perfil_id`) REFERENCES `perfis` (`id`),
  ADD CONSTRAINT `perfis_planos_plano_id_foreign` FOREIGN KEY (`plano_id`) REFERENCES `planos` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
