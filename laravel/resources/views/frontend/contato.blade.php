@extends('frontend.common.template')

@section('content')

    <div class="main contato">
        <div class="faixa">
            <h2 class="center">{{ trans('frontend.header.contato') }}</h2>
        </div>

        <div class="center">
            <div class="informacoes">
                <p class="telefone">{{ $contato->telefone }}</p>
                <p class="endereco">{!! $contato->{'endereco_'.app()->getLocale()} !!}</p>
            </div>

            <form action="" id="form-briefing" method="POST" enctype="multipart/form-data">
                <h3>{{ trans('frontend.contato.titulo-briefing') }}</h3>
                <input type="text" name="nome" id="briefing_nome" placeholder="{{ trans('frontend.contato.nome') }}" required>
                <input type="text" name="empresa" id="briefing_empresa" placeholder="{{ trans('frontend.contato.empresa') }}">
                <input type="email" name="email" id="briefing_email" placeholder="e-mail" required>
                <input type="text" name="telefone" id="briefing_telefone" placeholder="{{ trans('frontend.contato.telefone') }}">
                <textarea name="como_conheceu" id="briefing_como_conheceu" placeholder="{{ trans('frontend.contato.como-conheceu') }}" required></textarea>
                <label class="file-input" id="briefing">
                    <input type="file" name="briefing">
                    <span>{{ trans('frontend.contato.anexar-briefing') }}</span>
                </label>
                <input type="submit" value="{{ trans('frontend.contato.enviar') }}">
                <div id="form-briefing-response" class="response"></div>
            </form>

            <form action="" id="form-contato" method="POST" enctype="multipart/form-data">
                <h3>{{ trans('frontend.contato.titulo-contato') }}</h3>
                <input type="text" name="nome" id="contato_nome" placeholder="{{ trans('frontend.contato.nome') }}" required>
                <input type="email" name="email" id="contato_email" placeholder="e-mail" required>
                <input type="text" name="telefone" id="contato_telefone" placeholder="{{ trans('frontend.contato.telefone') }}">
                <textarea name="mensagem" id="contato_mensagem" placeholder="{{ trans('frontend.contato.mensagem') }}" required></textarea>
                <input type="submit" value="{{ trans('frontend.contato.enviar') }}">
                <div id="form-contato-response" class="response"></div>
            </form>

            <form action="" id="form-fornecedor" method="POST" enctype="multipart/form-data">
                <h3>{{ trans('frontend.contato.titulo-fornecedor') }}</h3>
                <input type="text" name="nome" id="fornecedor_nome" placeholder="{{ trans('frontend.contato.nome') }}" required>
                <input type="email" name="email" id="fornecedor_email" placeholder="e-mail" required>
                <input type="text" name="empresa" id="fornecedor_empresa" placeholder="{{ trans('frontend.contato.empresa') }}">
                <input type="text" name="telefone" id="fornecedor_telefone" placeholder="{{ trans('frontend.contato.telefone') }}">
                <textarea name="mensagem" id="fornecedor_mensagem" placeholder="{{ trans('frontend.contato.mensagem') }}" required></textarea>
                <input type="submit" value="{{ trans('frontend.contato.enviar') }}">
                <div id="form-fornecedor-response" class="response"></div>
            </form>

            <form action="" id="form-curriculo" method="POST" enctype="multipart/form-data">
                <h3>{{ trans('frontend.contato.titulo-curriculo') }}</h3>
                <input type="text" name="nome" id="curriculo_nome" placeholder="{{ trans('frontend.contato.nome') }}" required>
                <input type="email" name="email" id="curriculo_email" placeholder="e-mail" required>
                <input type="text" name="telefone" id="curriculo_telefone" placeholder="{{ trans('frontend.contato.telefone') }}">
                <textarea name="mensagem" id="curriculo_mensagem" placeholder="{{ trans('frontend.contato.mensagem') }}" required></textarea>
                <label class="file-input" id="curriculo">
                    <input type="file" name="curriculo">
                    <span>{{ trans('frontend.contato.anexar-curriculo') }}</span>
                </label>
                <input type="submit" value="{{ trans('frontend.contato.enviar') }}">
                <div id="form-curriculo-response" class="response"></div>
            </form>
        </div>

        <div class="mapa">
            {!! $contato->google_maps !!}
        </div>
    </div>

@endsection
