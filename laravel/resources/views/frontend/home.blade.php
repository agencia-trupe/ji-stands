@extends('frontend.common.template')

@section('content')

    <div class="banners">
        @foreach($banners as $banner)
        <a href="{{ $banner->link }}" class="banner">
            <div class="imagem" style="background-image: url('{{ asset('assets/img/banners/'.$banner->imagem) }}')"></div>
            <div class="faixa">
                <p class="center">{!! $banner->{'frase_'.app()->getLocale()} !!}</p>
            </div>
        </a>
        @endforeach
    </div>

    @if($popup->ativo)
    <a
        id="popup"
        href="{{ asset('assets/img/popup/'.$popup->imagem) }}"
        style="display:none"
        data-link="{{ $popup->link }}"
    ></a>
    @endif

@endsection
