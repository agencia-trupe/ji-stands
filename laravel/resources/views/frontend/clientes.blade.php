@extends('frontend.common.template')

@section('content')

    <div class="main clientes">
        <div class="faixa">
            <h2 class="center">{{ trans('frontend.header.clientes') }}</h2>
        </div>

        <div class="center clientes-thumbs">
            @foreach($clientes as $cliente)
                <div>
                    <img src="{{ asset('assets/img/clientes/'.$cliente->imagem) }}" alt="">
                </div>
            @endforeach
        </div>
    </div>

@endsection
