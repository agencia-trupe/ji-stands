@extends('frontend.common.template')

@section('content')

    <div class="main servicos">
        <div class="faixa">
            <h2 class="center">{{ trans('frontend.header.servicos') }}</h2>
        </div>

        <div class="center">
            <div class="servicos-categoria">
                <h2>{{ trans('frontend.projetos.stands') }}</h2>
                <img src="{{ asset('assets/img/servicos/'.$servicos->stands_imagem) }}" alt="">
                <div class="texto">
                    {!! $servicos->{'stands_'.app()->getLocale()} !!}
                </div>
                <a href="{{ route('projetos', 'stands') }}">{!! trans('frontend.servicos.ver-stands') !!}</a>
            </div>
            <div class="servicos-categoria">
                <h2>{{ trans('frontend.projetos.cenografia') }}</h2>
                <img src="{{ asset('assets/img/servicos/'.$servicos->cenografia_imagem) }}" alt="">
                <div class="texto">
                    {!! $servicos->{'cenografia_'.app()->getLocale()} !!}
                </div>
                <a href="{{ route('projetos', 'cenografia') }}">{!! trans('frontend.servicos.ver-cenografia') !!}</a>
            </div>
            <div class="servicos-segmentos">
                <h3>{{ trans('frontend.servicos.segmentos') }}</h3>
                <div>
                    @foreach($segmentos as $segmento)
                    <p>&raquo; {{ $segmento->{'titulo_'.app()->getLocale()} }} &laquo;</p>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
