@extends('frontend.common.template')

@section('content')

    <div class="main empresa">
        <div class="faixa">
            <h2 class="center">{{ trans('frontend.header.empresa') }}</h2>
        </div>

        <div class="center">
            <div class="secao secao-historia @if($secao === 'historia') aberto @endif">
                <a href="#" class="handle">{{ trans('frontend.empresa.historia') }}</a>
                <div class="secao-conteudo">
                    {!! $historia->{'texto_'.app()->getLocale()} !!}
                </div>
            </div>

            <div class="secao secao-diferenciais @if($secao === 'diferenciais') aberto @endif">
                <a href="#" class="handle">{{ trans('frontend.empresa.diferenciais') }}</a>
                <div class="secao-conteudo">
                    @foreach($diferenciais as $diferencial)
                    <div class="diferencial">
                        <img src="{{ asset('assets/img/diferenciais/'.$diferencial->imagem) }}" alt="">
                        <h4>{{ $diferencial->{'titulo_'.app()->getLocale()} }}</h4>
                        <p>{!! $diferencial->{'texto_'.app()->getLocale()} !!}</p>
                    </div>
                    @endforeach
                </div>
            </div>

            <div class="secao secao-valores @if($secao === 'valores') aberto @endif">
                <a href="#" class="handle">{{ trans('frontend.empresa.valores') }}</a>
                <div class="secao-conteudo">
                    <div class="col">
                        <img src="{{ asset('assets/img/layout/valores1.png') }}" alt="">
                        <h4>{{ trans('frontend.empresa.missao') }}</h4>
                        <p>{!! $valores->{'missao_'.app()->getLocale()} !!}</p>
                    </div>
                    <div class="col">
                        <img src="{{ asset('assets/img/layout/valores2.png') }}" alt="">
                        <h4>{{ trans('frontend.empresa.visao') }}</h4>
                        <p>{!! $valores->{'visao_'.app()->getLocale()} !!}</p>
                    </div>
                    <div class="col">
                        <img src="{{ asset('assets/img/layout/valores3.png') }}" alt="">
                        <h4>{{ trans('frontend.empresa.valores') }}</h4>
                        <p>{!! $valores->{'valores_'.app()->getLocale()} !!}</p>
                    </div>
                </div>
            </div>

            <div class="secao secao-estrutura @if($secao === 'estrutura') aberto @endif">
                <a href="#" class="handle">{{ trans('frontend.empresa.estrutura') }}</a>
                <div class="secao-conteudo">
                    @foreach($estruturas as $estrutura)
                    <div class="estrutura">
                        <img src="{{ asset('assets/img/estrutura/'.$estrutura->imagem) }}" alt="">
                        <h4>{{ $estrutura->{'titulo_'.app()->getLocale()} }}</h4>
                        <p>{!! $estrutura->{'texto_'.app()->getLocale()} !!}</p>
                    </div>
                    @endforeach
                </div>
            </div>

            <div class="secao secao-perfil @if($secao === 'perfil') aberto @endif">
                <a href="#" class="handle">{{ trans('frontend.empresa.perfil') }}</a>
                <div class="secao-conteudo">
                    <div class="perfil-imagem">
                        <div class="col">
                            <h4>Jens Dam</h4>
                            <p>{!! $perfil->{'jens_'.app()->getLocale()} !!}</p>
                        </div>
                        <div class="col">
                            <h4>Ivair Nunes</h4>
                            <p>{!! $perfil->{'ivair_'.app()->getLocale()} !!}</p>
                        </div>
                    </div>

                    <div class="perfil-depoimentos">
                        <p>{{ trans('frontend.empresa.depoimentos-titulo') }}</p>
                        <div>
                            @foreach($depoimentos as $depoimento)
                            <a href="{{ $depoimento->video_tipo === 'youtube' ? 'https://youtube.com/embed/'.$depoimento->video_codigo.'?autoplay=1' : 'https://player.vimeo.com/video/'.$depoimento->video_codigo }}" class="video">
                                <div class="capa" style="background-image:url('{{ asset('assets/img/depoimentos/'.$depoimento->capa) }}')"></div>
                                <p>
                                    {{ $depoimento->{'titulo_'.app()->getLocale()} }}
                                    <span>{{ $depoimento->{'descricao_'.app()->getLocale()} }}</span>
                                </p>
                            </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
