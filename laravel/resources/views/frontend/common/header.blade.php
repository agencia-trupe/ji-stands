    <header>
        <div class="center">
            <a href="{{ route('home') }}" class="logo">{{ config('site.name') }}</a>

            <nav id="nav-desktop">
                @include('frontend.common._nav')
            </nav>

            <div class="header-contato">
                <p>{{ $contato->telefone }}</p>
                <p>{!! $contato->{'endereco_'.app()->getLocale()} !!}</p>
            </div>

            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>

        <nav id="nav-mobile">
            @include('frontend.common._nav')
        </nav>
    </header>
