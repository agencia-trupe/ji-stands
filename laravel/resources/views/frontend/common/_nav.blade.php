<a href="{{ route('empresa') }}" @if(Route::currentRouteName() === 'empresa') class="active" @endif>{{ trans('frontend.header.empresa') }}</a>
<a href="{{ route('servicos') }}" @if(Route::currentRouteName() === 'servicos') class="active" @endif>{{ trans('frontend.header.servicos') }}</a>
<div class="menu-projetos">
    <a href="{{ route('projetos') }}" @if(Route::currentRouteName() === 'projetos') class="active" @endif>{{ trans('frontend.header.projetos') }}</a>
    <div class="subs">
        <a href="{{ route('projetos', 'stands') }}" @if(isset($categoria) && $categoria === 'stands') class="active" @endif">{{ trans('frontend.projetos.stands') }}</a>
        <a href="{{ route('projetos', 'cenografia') }}" @if(isset($categoria) && $categoria === 'cenografia') class="active" @endif>{{ trans('frontend.projetos.cenografia') }}</a>
    </div>
</div>
<a href="{{ route('clientes') }}" @if(Route::currentRouteName() === 'clientes') class="active" @endif>{{ trans('frontend.header.clientes') }}</a>
<a href="{{ route('contato') }}" @if(Route::currentRouteName() === 'contato') class="active" @endif>{{ trans('frontend.header.contato') }}</a>

<div>
    @include('frontend.common._social')
</div>

<?php $locale = app()->getLocale() === 'pt' ? 'en' : 'pt'; ?>
<a href="{{ route('lang', $locale) }}" class="lang lang-{{ $locale }}">
    {{ $locale === 'pt' ? 'Versão em Português' : 'English Version' }}
</a>
