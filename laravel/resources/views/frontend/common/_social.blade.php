@if($contato->facebook_link)
<a href="{{ $contato->facebook_link }}" target="_blank" class="social social-facebook">
    {{ $contato->facebook_texto }}
</a>
@endif
@if($contato->instagram_link)
<a href="{{ $contato->instagram_link }}" target="_blank" class="social social-instagram">
    {{ $contato->instagram_texto }}
</a>
@endif
@if($contato->youtube_link)
<a href="{{ $contato->youtube_link }}" target="_blank" class="social social-youtube">
    {{ $contato->youtube_texto }}
</a>
@endif
