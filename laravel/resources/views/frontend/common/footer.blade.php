    <footer>
        <div class="links">
            <div class="center">
                <div class="wrapper">
                    <div class="col">
                        <a href="{{ route('home') }}">Home</a>
                        <a href="{{ route('empresa') }}">{{ trans('frontend.header.empresa') }}</a>
                        <a href="{{ route('empresa', 'historia') }}" class="sub">&raquo; {{ trans('frontend.empresa.historia') }}</a>
                        <a href="{{ route('empresa', 'diferenciais') }}" class="sub">&raquo; {{ trans('frontend.empresa.diferenciais') }}</a>
                        <a href="{{ route('empresa', 'valores') }}" class="sub">&raquo; {{ trans('frontend.empresa.valores') }}</a>
                        <a href="{{ route('empresa', 'estrutura') }}" class="sub">&raquo; {{ trans('frontend.empresa.estrutura') }}</a>
                        <a href="{{ route('empresa', 'perfil') }}" class="sub">&raquo; {{ trans('frontend.empresa.perfil') }}</a>
                    </div>
                    <div class="col">
                        <a href="{{ route('servicos') }}">{{ trans('frontend.header.servicos') }}</a>
                        <a href="{{ route('projetos') }}">{{ trans('frontend.header.projetos') }}</a>
                        <a href="{{ route('projetos', 'stands') }}" class="sub">&raquo; {{ trans('frontend.projetos.stands') }}</a>
                        <a href="{{ route('projetos', 'cenografia') }}" class="sub">&raquo; {{ trans('frontend.projetos.cenografia') }}</a>
                        <a href="{{ route('clientes') }}">{{ trans('frontend.header.clientes') }}</a>
                    </div>
                    <div class="col">
                        <a href="{{ route('contato') }}">{{ trans('frontend.header.contato') }}</a>
                        <p class="telefone">{{ $contato->telefone }}</p>
                        @include('frontend.common._social')
                    </div>
                    <div class="col">
                        <img src="{{ asset('assets/img/layout/ji-stands-footer.png') }}" alt="">
                        <p class="endereco">{!! $contato->{'endereco_'.app()->getLocale()} !!}</p>
                    </div>
                    <div class="col col-newsletter">
                        <p>{{ trans('frontend.footer.newsletter') }}</p>
                        <form action="" id="form-newsletter" method="POST">
                            <input type="name" name="nome" id="newsletter_nome" placeholder="{{ trans('frontend.footer.nome') }}" required>
                            <input type="email" name="email" id="newsletter_email" placeholder="e-mail" required>
                            <div id="form-newsletter-response"></div>
                            <input type="submit" value="{{ trans('frontend.footer.enviar') }}">
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="center">
                <p>
                    © {{ date('Y') }} {{ config('site.name') }} - {{ trans('frontend.footer.copyright') }}.
                    <span>|</span>
                    <a href="http://trupe.net" target="_blank">{{ trans('frontend.footer.criacao') }}</a>:
                    <a href="http://trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
