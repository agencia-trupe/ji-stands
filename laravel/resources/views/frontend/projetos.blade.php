@extends('frontend.common.template')

@section('content')

    <div class="main projetos">
        <div class="faixa">
            <h2 class="center">{{ trans('frontend.projetos.'.$categoria) }}</h2>
        </div>

        <div class="center projetos-masonry">
            <div class="gutter-sizer"></div>
            <div class="grid-sizer"></div>
            @foreach($projetos as $projeto)
            <a href="#" class="thumb thumb-masonry" data-galeria="{{ $projeto->id }}" title="{{ $projeto->{'titulo_'.app()->getLocale()} }}">
                <img src="{{ asset('assets/img/projetos/'.$projeto->capa) }}" alt="" title="">
                <div class="overlay">
                    <span>{{ $projeto->{'titulo_'.app()->getLocale()} }}</span>
                </div>
            </a>
            @endforeach
        </div>
    </div>

    <div class="hidden">
    @foreach($projetos as $projeto)
        @foreach($projeto->imagens as $imagem)
            <a href="{{ asset('assets/img/projetos/imagens/'.$imagem->imagem) }}" class="fancybox" rel="galeria{{ $projeto->id }}" title="{{ $projeto->{'titulo_'.app()->getLocale()} }}"></a>
        @endforeach
    @endforeach
    </div>

@endsection
