@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Estrutura /</small> Adicionar Estrutura</h2>
    </legend>

    {!! Form::open(['route' => 'painel.estrutura.store', 'files' => true]) !!}

        @include('painel.estrutura.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
