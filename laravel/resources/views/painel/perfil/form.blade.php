@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('jens_pt', 'Jens PT') !!}
            {!! Form::textarea('jens_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'diferenciais']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('jens_en', 'Jens EN') !!}
            {!! Form::textarea('jens_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'diferenciais']) !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('ivair_pt', 'Ivair PT') !!}
            {!! Form::textarea('ivair_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'diferenciais']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('ivair_en', 'Ivair EN') !!}
            {!! Form::textarea('ivair_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'diferenciais']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
