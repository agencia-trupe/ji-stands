@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('telefone', 'Telefone') !!}
    {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('endereco_pt', 'Endereço PT') !!}
            {!! Form::textarea('endereco_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'endereco']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('endereco_en', 'Endereço EN') !!}
            {!! Form::textarea('endereco_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'endereco']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('google_maps', 'Código GoogleMaps') !!}
    {!! Form::text('google_maps', null, ['class' => 'form-control']) !!}
</div>

<hr>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('facebook_link', 'Facebook - Link (opcional)') !!}
            {!! Form::text('facebook_link', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('facebook_texto', 'Facebook - Texto (opcional)') !!}
            {!! Form::text('facebook_texto', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('instagram_link', 'Instagram - Link (opcional)') !!}
            {!! Form::text('instagram_link', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('instagram_texto', 'Instagram - Texto (opcional)') !!}
            {!! Form::text('instagram_texto', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('youtube_link', 'YouTube - Link (opcional)') !!}
            {!! Form::text('youtube_link', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('youtube_texto', 'YouTube - Texto (opcional)') !!}
            {!! Form::text('youtube_texto', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
