@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Briefings Recebidos</h2>
    </legend>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $contato->created_at }}</div>
    </div>

    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $contato->nome }}</div>
    </div>

@if($contato->empresa)
    <div class="form-group">
        <label>Empresa</label>
        <div class="well">{{ $contato->empresa }}</div>
    </div>
@endif

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">{{ $contato->email }}</div>
    </div>

@if($contato->telefone)
    <div class="form-group">
        <label>Telefone</label>
        <div class="well">{{ $contato->telefone }}</div>
    </div>
@endif

    <div class="form-group">
        <label>Como conheceu nossa empresa?</label>
        <div class="well">{{ $contato->como_conheceu }}</div>
    </div>

@if($contato->briefing)
    <div class="form-group">
        <label>Briefing</label>
        <div class="well">
            <a href="{{ url('briefings/'.$contato->briefing) }}" target="_blank">{{ $contato->briefing }}</a>
        </div>
    </div>
@endif

    <a href="{{ route('painel.contato.briefings.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
