@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Diferenciais /</small> Adicionar Diferencial</h2>
    </legend>

    {!! Form::open(['route' => 'painel.diferenciais.store', 'files' => true]) !!}

        @include('painel.diferenciais.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
