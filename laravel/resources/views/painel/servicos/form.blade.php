@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('stands_imagem', 'Stands Imagem') !!}
            <img src="{{ url('assets/img/servicos/'.$registro->stands_imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('stands_imagem', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('stands_pt', 'Stands PT') !!}
            {!! Form::textarea('stands_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'servicos']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('stands_en', 'Stands EN') !!}
            {!! Form::textarea('stands_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'servicos']) !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="well form-group">
            {!! Form::label('cenografia_imagem', 'Cenografia Imagem') !!}
            <img src="{{ url('assets/img/servicos/'.$registro->cenografia_imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
            {!! Form::file('cenografia_imagem', ['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('cenografia_pt', 'Cenografia PT') !!}
            {!! Form::textarea('cenografia_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'servicos']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('cenografia_en', 'Cenografia EN') !!}
            {!! Form::textarea('cenografia_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'servicos']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
