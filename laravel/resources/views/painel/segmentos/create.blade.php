@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Segmentos /</small> Adicionar Segmento</h2>
    </legend>

    {!! Form::open(['route' => 'painel.segmentos.store', 'files' => true]) !!}

        @include('painel.segmentos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
