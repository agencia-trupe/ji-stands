@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Segmentos /</small> Editar Segmento</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.segmentos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.segmentos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
