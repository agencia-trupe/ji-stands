@include('painel.common.flash')

<div class="form-group">
    {!! Form::checkbox('ativo', 1, null, [
        'class'       => 'form-control',
        'data-toggle' => 'toggle',
        'data-on'     => 'Ativo',
        'data-off'    => 'Inativo'
    ]) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    <img src="{{ url('assets/img/popup/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('link', 'Link (opcional)') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
