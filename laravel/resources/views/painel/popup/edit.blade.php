@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Popup</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.popup.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.popup.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
