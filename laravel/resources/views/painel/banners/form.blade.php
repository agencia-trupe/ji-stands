@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('frase_pt', 'Frase PT') !!}
    {!! Form::textarea('frase_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'banner']) !!}
</div>

<div class="form-group">
    {!! Form::label('frase_en', 'Frase EN') !!}
    {!! Form::textarea('frase_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'banner']) !!}
</div>

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
@if($submitText == 'Alterar')
    <img src="{{ url('assets/img/banners/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
@endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('link', 'Link') !!}
    {!! Form::text('link', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.banners.index') }}" class="btn btn-default btn-voltar">Voltar</a>
