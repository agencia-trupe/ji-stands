@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('missao_pt', 'Missão PT') !!}
            {!! Form::textarea('missao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'diferenciais']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('missao_en', 'Missão EN') !!}
            {!! Form::textarea('missao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'diferenciais']) !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('visao_pt', 'Visão PT') !!}
            {!! Form::textarea('visao_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'diferenciais']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('visao_en', 'Visão EN') !!}
            {!! Form::textarea('visao_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'diferenciais']) !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('valores_pt', 'Valores PT') !!}
            {!! Form::textarea('valores_pt', null, ['class' => 'form-control ckeditor', 'data-editor' => 'diferenciais']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('valores_en', 'Valores EN') !!}
            {!! Form::textarea('valores_en', null, ['class' => 'form-control ckeditor', 'data-editor' => 'diferenciais']) !!}
        </div>
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
