@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Valores</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.valores.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.valores.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
