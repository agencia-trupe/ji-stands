<ul class="nav navbar-nav">
	<li @if(str_is('painel.popup*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.popup.index') }}">Popup</a>
	</li>
    <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>
    <li class="dropdown @if(str_is('painel.historia*', Route::currentRouteName()) || str_is('painel.diferenciais*', Route::currentRouteName()) || str_is('painel.valores*', Route::currentRouteName()) || str_is('painel.estrutura*', Route::currentRouteName()) || str_is('painel.perfil*', Route::currentRouteName()) || str_is('painel.depoimentos*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Empresa
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li>
                <a href="{{ route('painel.historia.index') }}">História</a>
            </li>
            <li>
                <a href="{{ route('painel.diferenciais.index') }}">Diferenciais</a>
            </li>
            <li>
                <a href="{{ route('painel.valores.index') }}">Valores</a>
            </li>
        	<li>
        		<a href="{{ route('painel.estrutura.index') }}">Estrutura</a>
        	</li>
            <li>
                <a href="{{ route('painel.perfil.index') }}">Perfil</a>
            </li>
            <li>
                <a href="{{ route('painel.depoimentos.index') }}">Depoimentos</a>
            </li>
        </ul>
    </li>
    <li class="dropdown @if(str_is('painel.servicos*', Route::currentRouteName()) || str_is('painel.segmentos*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Serviços
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li>
                <a href="{{ route('painel.servicos.index') }}">Serviços</a>
            </li>
            <li>
                <a href="{{ route('painel.segmentos.index') }}">Segmentos</a>
            </li>
        </ul>
    </li>
    <li @if(str_is('painel.projetos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.projetos.index') }}">Projetos</a>
    </li>
	<li @if(str_is('painel.clientes*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.clientes.index') }}">Clientes</a>
	</li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos + $fornecedoresNaoLidos + $curriculosNaoLidos + $briefingsNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos + $fornecedoresNaoLidos + $curriculosNaoLidos + $briefingsNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato.index') }}">Informações de Contato</a></li>
            <li class="divider"></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
            <li><a href="{{ route('painel.contato.fornecedores.index') }}">
                Fornecedores Recebidos
                @if($fornecedoresNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $fornecedoresNaoLidos }}</span>
                @endif
            </a></li>
            <li><a href="{{ route('painel.contato.curriculos.index') }}">
                Currículos Recebidos
                @if($curriculosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $curriculosNaoLidos }}</span>
                @endif
            </a></li>
            <li><a href="{{ route('painel.contato.briefings.index') }}">
                Briefings Recebidos
                @if($briefingsNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $briefingsNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
    <li @if(str_is('painel.newsletter*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.newsletter.index') }}">Newsletter</a>
    </li>
</ul>
