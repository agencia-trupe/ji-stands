@include('painel.common.flash')

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_pt', 'Título PT') !!}
            {!! Form::text('titulo_pt', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('titulo_en', 'Título EN') !!}
            {!! Form::text('titulo_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('descricao_pt', 'Descrição PT') !!}
            {!! Form::text('descricao_pt', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('descricao_en', 'Descrição EN') !!}
            {!! Form::text('descricao_en', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('video_tipo', 'Vídeo Tipo') !!}
            {!! Form::select('video_tipo', ['youtube' => 'YouTube', 'vimeo' => 'Vimeo'], null, ['class' => 'form-control', 'placeholder' => 'Selecione']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            {!! Form::label('video_codigo', 'Vídeo Código') !!}
            {!! Form::text('video_codigo', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.depoimentos.index') }}" class="btn btn-default btn-voltar">Voltar</a>
