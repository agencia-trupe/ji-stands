<?php

return [

    'header' => [
        'empresa'  => 'Company',
        'servicos' => 'Services',
        'projetos' => 'Projects',
        'clientes' => 'Customers',
        'contato'  => 'Contact'
    ],

    'footer' => [
        'copyright'  => 'All rights reserved',
        'newsletter' => 'Sign up to receive news:',
        'nome'       => 'name',
        'enviar'     => 'Send',
        'criacao'    => 'Creation of websites'
    ],

    'empresa' => [
        'historia'           => 'History',
        'diferenciais'       => 'Diferentials',
        'valores'            => 'Values',
        'estrutura'          => 'Structure',
        'perfil'             => 'Profile',
        'missao'             => 'Mission',
        'visao'              => 'Vision',
        'valores'            => 'Values',
        'depoimentos-titulo' => 'Our clientes and our operation talk more about our profile'
    ],

    'servicos' => [
        'ver-stands'     => 'see <span>stands projects</span>',
        'ver-cenografia' => 'see <span>scenography projects</span>',
        'segmentos'      => 'segments'
    ],

    'projetos' => [
        'stands'     => 'Stands',
        'cenografia' => 'Scenography'
    ],

    'contato' => [
        'nome'              => 'name',
        'empresa'           => 'company',
        'telefone'          => 'telephone',
        'mensagem'          => 'message',
        'enviar'            => 'send',
        'sucesso'           => 'Message sent with success!',
        'erro'              => 'Please fill in all information correctly',
        'como-conheceu'     => 'how did you hear about us?',
        'anexar-briefing'   => 'ATTACH BRIEFING',
        'anexar-curriculo'  => 'ATTACH CURRICULUM',
        'titulo-briefing'   => 'SEND YOUR BRIEFING',
        'titulo-contato'    => 'CONTACT US',
        'titulo-fornecedor' => 'SUPPLIERS',
        'titulo-curriculo'  => 'WORK WITH US'
    ]

];
