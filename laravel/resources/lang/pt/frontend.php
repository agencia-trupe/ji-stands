<?php

return [

    'header' => [
        'empresa'  => 'Empresa',
        'servicos' => 'Serviços',
        'projetos' => 'Projetos',
        'clientes' => 'Clientes',
        'contato'  => 'Contato'
    ],

    'footer' => [
        'copyright'  => 'Todos os direitos reservados',
        'newsletter' => 'Cadastre-se para receber novidades:',
        'nome'       => 'nome',
        'enviar'     => 'Enviar',
        'criacao'    => 'Criação de sites'
    ],

    'empresa' => [
        'historia'           => 'História',
        'diferenciais'       => 'Diferenciais',
        'valores'            => 'Valores',
        'estrutura'          => 'Estrutura',
        'perfil'             => 'Perfil',
        'missao'             => 'Missão',
        'visao'              => 'Visão',
        'valores'            => 'Valores',
        'depoimentos-titulo' => 'Nossos clientes e nossa operação falam mais sobre nosso perfil'
    ],

    'servicos' => [
        'ver-stands'     => 'ver projetos de <span>Stands</span>',
        'ver-cenografia' => 'ver projetos de <span>Cenografia</span>',
        'segmentos'      => 'Segmentos'
    ],

    'projetos' => [
        'stands'     => 'Stands',
        'cenografia' => 'Cenografia'
    ],

    'contato' => [
        'nome'              => 'nome',
        'empresa'           => 'empresa',
        'telefone'          => 'telefone',
        'mensagem'          => 'mensagem',
        'enviar'            => 'enviar',
        'sucesso'           => 'Mensagem enviada com sucesso!',
        'erro'              => 'Preencha todos os campos corretamente',
        'como-conheceu'     => 'como conheceu nossa empresa?',
        'anexar-briefing'   => 'ANEXAR BRIEFING',
        'anexar-curriculo'  => 'ANEXAR CURRÍCULO',
        'titulo-briefing'   => 'ENVIE SEU BRIEFING',
        'titulo-contato'    => 'FALE CONOSCO',
        'titulo-fornecedor' => 'FORNECEDORES',
        'titulo-curriculo'  => 'TRABALHE CONOSCO'
    ]

];
